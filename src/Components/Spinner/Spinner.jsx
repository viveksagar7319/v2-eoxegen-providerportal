import React from "react";
import "./styles.css";

const Spinner = () => {
    return <div className="loader"></div>;
};

export default Spinner;
