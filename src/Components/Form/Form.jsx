import React, { useState, useRef } from "react";
import "./styles.css";
import { Login, verifyWithOTP } from "../../API/login";
import cogoToast from "cogo-toast";
import Base64 from "crypto-js/enc-base64";
import Utf8 from "crypto-js/enc-utf8";
import AES from "crypto-js/aes";
import CryptoJSCore from "crypto-js/core";
import Pkcs7 from "crypto-js/pad-pkcs7";
import Spinner from "../Spinner/Spinner";
import lock from "../../assets/lock.svg";
import { Link } from "react-router-dom";
import { getResultFromData } from "../../utils/utils";
// import { useNavigate } from "react-router-dom";
import { useAuth } from "../../hooks/useAuth";
import { useEffect } from "react";
import { CHECK_MSG } from "../../utils/constants";
import GALogo from "../../assets/logoGA.jpg";
import { Step1, Step2, Step3 } from "./RegistartionSteps";
import { set } from "lodash";
import { SaveProviderEmpanellment } from "../../API/registarion.api";

const LoginForm = ({ handleBackGroundBlur }) => {
    const [loading, setLoading] = useState(false);
    const [userID, setUserID] = useState();
    const [OTP, setOTP] = useState("");
    const modalRef = useRef(null);
    const modelRefFormDialog = useRef(null);
    const OTPref = useRef(null);
    const [handleStep, setHandleStep] = useState(1);

    const [finalSubmit, setFinalSubmit] = useState(false);

    // handle Step1 Object
    const [step1, setStep1] = useState({
        providerType: null,
        providerName: null,
        panNo: null,
        providerAddLine1: null,
        providerAddLine2: null,
        providerPin: null,
        providerCountry: null,
        providerState: null,
        providerCity: null,
        hospitalLatitude: "22.966200",
        hospitalLongitude: "88.389359",
        providerAddLocation: null,
        providerCategory: "Z",
        tpaCode: "999",
    });

    // handle Step2 Object
    const [step2, setStep2] = useState({
        providerRegNo: null,
        providerContactPerson: null,
        hospRegistrationValidUpto: null,
        hospRohiniValidUpto: null,
        providerEmail: null,
        providerEmail1: null,
        providerEmail2: null,
        providerPhNo: null,
        providerPhNo1: null,
        providerPhNo2: null,
        providerRohini: null,
        providerWebsite: null,
    });

    //  handle step3 Object
    const [step3, setStep3] = useState({
        certificates: null,
        specializations: null,
        servives: null,
        uploadCertificate: null,
    });

    useEffect(() => {
        finalSubmit && loadFinalSubmit();
    }, [finalSubmit]);

    useEffect(() => {
        const keyDownHandler = (event) => {
            if (event.key === "Escape" && modelRefFormDialog?.current?.open) {
                event.preventDefault();
                modelRefFormDialog?.current?.close();
                handleBackGroundBlur();
            }
        };

        document.addEventListener("keydown", keyDownHandler);

        return () => {
            document.removeEventListener("keydown", keyDownHandler);
        };
    }, []);

    const loadFinalSubmit = async () => {
        const testingPayload = {
            provider: {
                providerType: "Hospital",
                providerName: "Indian Heart Hospital",
                providerCategory: "Z",
                panNo: "AFKBR7799A",
                providerCountry: "IN",
                providerState: "IN.WB",
                providerCity: "IN.WB.KOLK029",
                providerPin: "700120",
                providerAddLine1: "139 Tampines St. 11",
                providerAddLine2: "#01-62 ",
                providerAddLocation: "Barrackpore Bazar",
                providerRegNo: "22D0052/01/223",
                hospRegistrationValidUpto: "31/12/2024",
                providerContactPerson: "JOY",
                providerPhNo: "9999999999",
                providerPhNo1: "8888888888",
                providerPhNo2: "7777777777",
                providerWebsite: "www.test.com",
                providerEmail: "xxxx@yahoo.com",
                providerEmail1: "yyyy@yahoo.com",
                providerEmail2: "zzzz@yahoo.com",
                providerRohini: "8900080000377",
                hospRohiniValidUpto: "31/12/2024",
                tpaCode: "999",
                hospitalLatitude: "22.966200",
                hospitalLongitude: "88.389359",
                certificates: [
                    {
                        certificate: "NABH - National Accreditation Board for Hospitals and Healthcare  Providers",
                    },
                ],
                servives: [
                    {
                        serviceName: "SER-A",
                    },
                    {
                        serviceName: "SER-B",
                    },
                ],
                specializations: [
                    {
                        specializationName: "SPL-A",
                    },
                    {
                        specializationName: "SPL-B",
                    },
                ],
            },
        };

        const payload = {
            provider: {
                ...JSON.parse(JSON.stringify(step1)),
                ...JSON.parse(JSON.stringify(step2)),
                ...JSON.parse(JSON.stringify(step3)),
            },
        };
        delete payload?.provider?.uploadCertificate;

        const result = await SaveProviderEmpanellment(payload);

        if (result?.data?.data) {
            cogoToast.success(result?.data?.data?.result?.message);
            setFinalSubmit(false);
            modelRefFormDialog.current.close();
        } else {
            cogoToast.error(result?.error?.response?.data?.message);
            setFinalSubmit(false);
        }
    };

    // const navigate = useNavigate();
    const { login } = useAuth();

    /**
     *
     * @param {*} e => onClick event with the form data
     */
    const handleSubmit = async (e) => {
        e.preventDefault();

        const { email, password } = e.target.form.elements;
        if (email.value.length > 0 && password.value.length > 0) {
            setUserID(email.value);
            const payLoad = {
                searchvalue: email.value,
                password: encryptPassword(password.value),
                sendType: "M",
            };
            e.target.disabled = true;
            setLoading(true);
            const data = await Login(payLoad);

            if (data.ok) {
                const result = getResultFromData(data);
                if (result) {
                    cogoToast.success(<p style={{ textAlign: "center" }}>{CHECK_MSG}</p>).then(() => modalRef.current.showModal());
                } else {
                    cogoToast.error(<p style={{ textAlign: "center" }}>Wrong Credentials</p>);
                }
                e.target.disabled = false;
                setLoading(false);
            } else {
                cogoToast.error("Login Error");
                e.target.disabled = false;
                setLoading(false);
            }
        } else {
            cogoToast.error("Please Provide Credentials");
        }
    };

    /**
     *
     * @param {*} password => User input password
     * @returns Hashed password
     */
    const encryptPassword = (password) => {
        const publickey = Base64.parse(import.meta.env.VITE_hd_publicKey);
        const algoKey = Base64.parse(import.meta.env.VITE_hd_algoKey);
        const utfStringified = Utf8.parse(password).toString();
        const aesEncrypted = AES.encrypt(utfStringified, publickey, {
            mode: CryptoJSCore.mode.CBC,
            padding: Pkcs7,
            iv: algoKey,
        });
        return aesEncrypted.ciphertext.toString(Base64);
    };

    /**
     *
     * @function : Verifies OTP and logs the user in
     */
    const handleOTP = async () => {
        if (String(OTP).length !== 6) {
            cogoToast.error("OTP must be 6 digits");
            return;
        }

        const payLoad = {
            searchvalue: userID,
            OTP: OTP,
        };

        OTPref.current.disabled = true;
        const data = await verifyWithOTP(payLoad);

        if (data.ok) {
            const result = getResultFromData(data);
            login(result); //Storing in Login Context
            // cogoToast.success("Successfully Logged In");
            setOTP("");
            modalRef.current.close();
            OTPref.current.disabled = false;
        } else {
            cogoToast.error("Incorrect OTP");
            OTPref.current.disabled = false;
        }
    };

    useEffect(() => {
        if (OTP.toString().length === 6) {
            handleOTP();
        }
    }, [OTP]);

    const handleRegister = (e) => {
        e.preventDefault();
        modelRefFormDialog.current.showModal();
        handleBackGroundBlur();
    };

    const closeRegistrationFormModule = () => {
        handleBackGroundBlur();
        modelRefFormDialog.current.close();
        setHandleStep(1);
        setStep1({
            providerType: null,
            providerName: null,
            panNo: null,
            providerAddLine1: null,
            providerAddLine2: null,
            providerPin: null,
            providerCountry: null,
            providerState: null,
            providerCity: null,
            hospitalLatitude: "22.966200",
            hospitalLongitude: "88.389359",
            providerAddLocation: null,
            providerCategory: "Z",
            tpaCode: "999",
        });
        setStep2({
            providerRegNo: null,
            providerContactPerson: null,
            hospRegistrationValidUpto: null,
            hospRohiniValidUpto: null,
            providerEmail: null,
            providerEmail1: null,
            providerEmail2: null,
            providerPhNo: null,
            providerPhNo1: null,
            providerPhNo2: null,
            providerRohini: null,
            providerWebsite: null,
        });
        setStep3({
            certificates: null,
            specializations: null,
            servives: null,
            uploadCertificate: null,
        });
    };

    const changeStep = (value) => {
        switch (value) {
            case 1:
                setHandleStep(value);
                return;
            case 2: {
                if (
                    !step1?.providerType ||
                    !step1?.providerName ||
                    !step1?.panNo ||
                    !step1?.providerAddLine1 ||
                    !step1?.providerPin ||
                    !step1?.providerCity ||
                    !step1?.providerCountry ||
                    !step1?.providerState ||
                    !step1?.providerAddLocation
                ) {
                    cogoToast.info("All fields are mandatory!");
                    return;
                } else {
                    setHandleStep(value);
                    return;
                }
            }

            case 3: {
                if (
                    !step2?.providerRegNo ||
                    !step2?.providerContactPerson ||
                    !step2?.hospRegistrationValidUpto ||
                    !step2?.hospRohiniValidUpto ||
                    !step2?.providerPhNo ||
                    !step2?.providerRohini
                ) {
                    cogoToast.info("All fields are mandatory!");
                    return;
                } else {
                    setHandleStep(value);
                    return;
                }
            }

            default: {
                console.log("Invalid step");
                return;
            }
        }
    };

    // Step1 handler
    const handleSteps1Check = (step1_Object) => {
        // CheckStep1 Object before going to set the state

        // Validating Object
        setStep1((prev) => {
            return { ...prev, ...step1_Object };
        });
        setHandleStep((prevValue) => prevValue + 1);
    };
    // Step2 handler
    const handleStep2Check = (step2_Object) => {
        setStep2({
            ...step2_Object,
        });

        setHandleStep((prevValue) => prevValue + 1);
    };

    // Step3 handler
    const handleStep3Check = (step3_Object) => {
        setStep3({
            ...step3_Object,
        });
        setFinalSubmit(true);
    };

    return (
        <main className="__main">
            <div className="form__content">
                <img src={GALogo} alt="" width={400} style={{ marginBottom: "3rem" }} />

                <h2>Welcome to GA Insurance Provider Portal</h2>
                <h4>Please sign-in to your account</h4>

                {/* This Form Section */}
                <form className="form__content" autoComplete="on" style={{ alignItems: "flex-start" }}>
                    <label htmlFor="email" />
                    USER ID
                    <input id="email" name="email" type="email" className="input" />
                    <label htmlFor="password" />
                    PASSWORD
                    <input id="password" name="password" type="password" className="input" />
                    <br />
                    {/* <div>
            <input type="checkbox" name="remember" id="remember" />
            <span>
              <label htmlFor="remember">&nbsp;Remember me</label>
            </span>
          </div> */}
                    {/* Login Button */}
                    <button
                        id="submit"
                        className="btn btn-primary"
                        onClick={handleSubmit}
                        style={{ minHeight: "2.2rem", zIndex: "1" }}
                        onKeyDown={(e) => (e.key === "Enter" ? handleSubmit : void 0)}
                    >
                        {loading ? <Spinner /> : "Login"}
                    </button>
                    <section className="forgot__passsword">
                        <Link to="/forgot" className="text__muted">
                            <img src={lock} alt="" />
                            Forgot Password?
                        </Link>
                    </section>
                    {/* Register */}
                    <button className="btn custom-btn" onClick={handleRegister}>
                        {"Register"}
                    </button>
                    <p>
                        <span>By clicking here, you agree to our</span>
                        <Link to="/" className="text__muted" id="customer_link">
                            <span>Customer Agreement</span>
                        </Link>
                    </p>
                </form>

                {/* This is Dialog Box for Entering OTP */}
                <dialog ref={modalRef} className="dialog__modal">
                    <section>
                        <label htmlFor="otp" />
                        Enter OTP
                        <input id="otp" name="otp" type="number" value={OTP} className="input" onChange={(e) => setOTP(e.target.valueAsNumber)} />
                        <button onClick={handleOTP} ref={OTPref} data-otp="OTP" className="btn btn-primary">
                            Submit
                        </button>
                    </section>
                </dialog>

                {/* This is Dialog for the form */}
                <dialog ref={modelRefFormDialog} className="Registration_form_modal hiddenScroll">
                    {/* Close Button */}
                    <section style={{ marginTop: "10px", display: "flex", justifyContent: "flex-end" }}>
                        <button className="btn btn-parent" style={{ width: "50px" }} onClick={closeRegistrationFormModule}>
                            ESC
                        </button>
                    </section>
                    <section className="after_dialog">
                        {/* Below section is handling pagination of the form */}
                        <section
                            className="form_steps"
                            style={{
                                // border: "1px solid green",
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "space-evenly",
                                gap: "50px",
                                // justifyContent: "space-evenly",
                                // alignItems: "center",
                                // border: "0.01px solid #D3D3D3",
                                // borderRadius: "10px",
                                padding: "10px",
                                // marginTop: "1rem",
                            }}
                        >
                            <div className={`common ${handleStep === 1 ? "paginationOnDemand" : "pagination"}`} onClick={() => changeStep(1)}>
                                1
                            </div>
                            <div className={`common ${handleStep === 2 ? "paginationOnDemand" : "pagination"}`} onClick={() => changeStep(2)}>
                                2
                            </div>
                            <div className={`common ${handleStep === 3 ? "paginationOnDemand" : "pagination"}`} onClick={() => changeStep(3)}>
                                3
                            </div>
                        </section>

                        {/* This is Our Changing form onClick Section */}
                        <section className="main_section_regitration hiddenScroll">
                            {/* {handleStep === 1 ? (
                                <Step1 handleSteps1Check={handleSteps1Check} />
                            ) : handleStep === 2 ? (
                                <Step2 handleStep2Check={handleStep2Check} />
                            ) : (
                                <Step3 handleStep3Check={handleStep3Check} finalSubmit={finalSubmit} />
                            )} */}

                            <div style={{ display: handleStep === 1 ? "block" : "none", height: "100%" }}>
                                {" "}
                                <Step1 handleSteps1Check={handleSteps1Check} />
                            </div>
                            <div style={{ display: handleStep === 2 ? "block" : "none", height: "100%" }}>
                                {" "}
                                <Step2 handleStep2Check={handleStep2Check} />
                            </div>
                            <div
                                style={{
                                    display: handleStep === 3 ? "block" : "none",
                                }}
                            >
                                <Step3 handleStep3Check={handleStep3Check} finalSubmit={finalSubmit} />
                            </div>

                            {/* <section style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}>
                                <button
                                    className="btn btn-primary"
                                    style={{ width: "100px" }}
                                    onClick={() => {
                                        setHandleStep((oldValue) => {
                                            if (oldValue === 3) {
                                                return oldValue;
                                            }
                                            return oldValue + 1;
                                        });
                                    }}
                                >
                                    {handleStep === 3 ? "Submit" : "Next"}
                                </button>
                            </section> */}
                        </section>
                    </section>
                </dialog>
            </div>
        </main>
    );
};
export default LoginForm;
