import React, { useEffect, useState } from "react";
import { Container, Row, Col, Form, Button, Card, Spinner, Badge, Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { Populatelocation, PopulatetypesValue } from "../../API/registarion.api";
import cogoToast from "cogo-toast";
import AsyncSelect from "react-select/async";

export const Step1 = ({ handleSteps1Check }) => {
    // Local

    const [step1Form, setStep1Form] = useState({
        providerTypeValue: null,
        pinCode: null,
        providerTypeArray: null,
    });

    const [location, setLocation] = useState({
        country: null,
        state: null,
        city: null,
        location: null,
    });

    useEffect(() => {
        loadProviderType();
    }, []);

    useEffect(() => {
        if (step1Form?.pinCode) {
            if (step1Form?.pinCode?.length === 6) {
                loadLoactionData(step1Form?.pinCode);
            }
        }
    }, [step1Form]);

    // Loading location basis of pin code
    const loadLoactionData = async (pinCode) => {
        const result = await Populatelocation({ pincodeNo: pinCode });
        const data = result?.data?.data?.result;

        if (data) {
            setLocation({
                country: data?.countryName,
                countryCode: data?.countryCode,
                state: data?.stateName,
                stateCode: data?.stateCode,
                city: data?.cityName,
                cityCode: data?.cityCode,
                location: data?.areaLocation,
            });
        } else {
            setLocation({
                country: null,
                state: null,
                city: null,
                location: null,
            });
        }
    };

    // Loading ProviderType
    const loadProviderType = async () => {
        const result = await PopulatetypesValue({ type: "PROVIDERTYPE" });
        const data = result?.data?.data?.result;

        if (data) {
            setStep1Form((prevValue) => {
                return {
                    ...prevValue,
                    providerTypeArray: data,
                };
            });
        }
    };

    const handleChange = (e, key) => {
        setStep1Form((oldObj) => {
            return {
                ...oldObj,
                [key]: e.target.value,
            };
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const formDataFromEvent = new FormData(e.target);

        let formObj = {};
        formDataFromEvent.forEach((value, key) => {
            formObj[key] = value;
        });

        formObj = {
            ...formObj,
            providerCountry: location?.countryCode,
            providerState: location?.stateCode,
            providerCity: location?.cityCode,
            providerAddLocation: location?.location,
        };
        handleSteps1Check(formObj);
    };

    return (
        <section style={{ height: "100%" }} className="common_section">
            <Form className="formStep1" onSubmit={handleSubmit}>
                {/*  Provider Type */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Provider Type <span className="mandate_field">*</span>
                    </Form.Label>
                    <Form.Select
                        required
                        // aria-multiselectable
                        name="providerType"
                        value={step1Form?.providerType}
                        onChange={(e) => handleChange(e, "providerTypeValue")}
                        style={{ cursor: "pointer" }}
                    >
                        <option value="">Select...</option>s
                        {step1Form?.providerTypeArray?.map((obj, index) => {
                            return (
                                <option key={index} value={obj.providerType}>
                                    {obj.providerType}
                                </option>
                            );
                        })}
                    </Form.Select>
                </Form.Group>

                {/* Provider Name */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Provider Name <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        name="providerName"
                        required
                        // //value={"12345"}
                        type="text"
                        placeholder="Enter Provider Name"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Provider Category */}
                {/* <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Provider Category <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerCategory"
                        value={"Z"}
                        disabled
                        type="text"
                        placeholder="Z"
                        maxLength={25}
                    />
                </Form.Group> */}

                {/* Pan No. */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Pan No. <span className="mandate_field">*</span>
                    </Form.Label>
                    <Form.Control
                        // onKeyDown={0}
                        name="panNo"
                        required
                        // //value={"12345"}
                        type="text"
                        placeholder="Enter Pan No"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Address Line 1 */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Address Line 1. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        // //value={"12345"}
                        name="providerAddLine1"
                        type="text"
                        placeholder="Enter Address Line 1"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Address Location 2 */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Address Line 2.</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        // required={false}
                        name="providerAddLine2"
                        type="text"
                        placeholder="Enter Address Location 2"
                        maxLength={25}
                    />
                </Form.Group>

                {/* PIN NO */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Pin Code. <span className="mandate_field">*</span>
                    </Form.Label>
                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerPin"
                        value={step1Form?.pinCode}
                        onChange={(e) => handleChange(e, "pinCode")}
                        type="text"
                        placeholder="Enter Pin Code"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Country */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Country. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        name="providerCountry"
                        required
                        disabled={step1Form?.pinCode?.length === 6}
                        value={step1Form?.pinCode?.length === 6 ? location?.country : ""}
                        type="text"
                        placeholder="Enter Country"
                        maxLength={25}
                    />
                </Form.Group>

                {/* State */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        State <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        name="providerState"
                        required
                        disabled={step1Form?.pinCode?.length === 6}
                        value={step1Form?.pinCode?.length === 6 ? location?.state : ""}
                        type="text"
                        placeholder="Enter State"
                        maxLength={25}
                    />
                </Form.Group>
                {/* City */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        City. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        name="providerCity"
                        required
                        disabled={step1Form?.pinCode?.length === 6}
                        value={step1Form?.pinCode?.length === 6 ? location?.city : ""}
                        type="text"
                        placeholder="Enter City"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Location */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Location <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        name="providerAddLocation"
                        disabled={step1Form?.pinCode?.length === 6}
                        required
                        value={step1Form?.pinCode?.length === 6 ? location?.location : ""}
                        type="text"
                        placeholder="Enter Location"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Lattitude And Altitude */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Lattitude And Altitude</Form.Label>
                    <Form.Control
                        // onKeyDown={0}
                        required={false}
                        ////value={"12345"}
                        type="text"
                        placeholder="Enter Lattitude And Altitude"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Lattitude And Altitude */}
                <Form.Group className="mt-4  col-lg-5 test_step1" style={{ visibility: "hidden" }}>
                    <Form.Label className="title_register">Lattitude And Altitude</Form.Label>
                    <Form.Control
                        // onKeyDown={0}
                        required={false}
                        ////value={"12345"}
                        type="text"
                        placeholder="Enter Lattitude And Altitude"
                        maxLength={25}
                    />
                </Form.Group>
                {/* Next */}
                <section className="next_button">
                    <button type="submit" className="btn btn-primary" style={{ width: "100px" }}>
                        {"Next"}
                    </button>
                </section>
            </Form>
        </section>
    );
};

export const Step2 = ({ handleStep2Check }) => {
    const handleSubmit = (e) => {
        e.preventDefault();

        console.log("Step2");

        //extract form data
        const step2Form = new FormData(e.target);
        let obj = {};

        step2Form.forEach((value, key) => {
            obj[key] = value;
        });

        const date1 = obj?.hospRegistrationValidUpto.split("-");
        const date2 = obj?.hospRohiniValidUpto.split("-");

        const newDate1 = date1[2] + "/" + date1[1] + "/" + date1[0];
        const newDate2 = date2[2] + "/" + date2[1] + "/" + date2[0];

        const dateOperation = { ...obj, hospRegistrationValidUpto: newDate1, hospRohiniValidUpto: newDate2 };

        handleStep2Check(dateOperation);
    };
    return (
        <section style={{ height: "100%" }} className="common_section">
            <Form className="formStep1" onSubmit={handleSubmit}>
                {/* Registration No  */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Registration No. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerRegNo"
                        ////value={"12345"}
                        type="text"
                        placeholder="Enter Registration No"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Contact Person */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Contact Person. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerContactPerson"
                        ////value={"12345"}
                        type="text"
                        placeholder="Enter Contact Person"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Registration Upto Date */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Registration Upto Date. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="hospRegistrationValidUpto"
                        ////value={"12345"}
                        type="date"
                        placeholder="Enter Member No."
                        maxLength={25}
                    />
                </Form.Group>

                {/* Phone No. */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Phone No. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerPhNo"
                        //value={"12345"}
                        type="text"
                        placeholder="Enter Phone No"
                        maxLength={10}
                    />
                </Form.Group>

                {/*  Phone No 1 */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Phone No 1.</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        // required
                        name="providerPhNo1"
                        //value={"12345"}
                        type="text"
                        placeholder="Enter Phone No 1"
                        maxLength={10}
                    />
                </Form.Group>

                {/*  Phone No 2. */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Phone No 2</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerPhNo2"
                        //value={"12345"}
                        type="text"
                        placeholder="Enter Phone No 2"
                        maxLength={10}
                    />
                </Form.Group>

                {/* Email */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Email.</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required={false}
                        name="providerEmail"
                        //value={"12345"}
                        type="email"
                        placeholder="Enter Email"
                        maxLength={100}
                    />
                </Form.Group>

                {/* Email 1 */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Email 1.</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required={false}
                        name="providerEmail1"
                        //value={"12345"}
                        type="email"
                        placeholder="Enter Email 1"
                        maxLength={100}
                    />
                </Form.Group>

                {/*  Email 2 */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Email 2.</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required={false}
                        name="providerEmail2"
                        //value={"12345"}
                        type="email"
                        placeholder="Enter Email 2"
                        maxLength={1000}
                    />
                </Form.Group>

                {/* Website */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Website</Form.Label>

                    <Form.Control
                        required={false} //value={"12345"}
                        type="text"
                        name="providerWebsite"
                        placeholder="Enter Website"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Rohini Code */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Rohini Code. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="providerRohini"
                        //value={"12345"}
                        type="text"
                        placeholder="Enter Rohini Code"
                        maxLength={25}
                    />
                </Form.Group>

                {/* Rohini Valid Upto */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Rohini Valid Upto. <span className="mandate_field">*</span>
                    </Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        required
                        name="hospRohiniValidUpto"
                        ////value={"12345"}
                        type="date"
                        placeholder="Enter Member No."
                        maxLength={25}
                    />
                </Form.Group>

                {/* Next */}
                <section className="next_button">
                    <button type="submit" className="btn btn-primary" style={{ width: "100px" }}>
                        {"Next"}
                    </button>
                </section>
            </Form>
        </section>
    );
};

export const Step3 = ({ handleStep3Check, finalSubmit }) => {
    const [certificates, setCertificates] = useState([]);
    const [specilizeList, setSpecilizeList] = useState([]);
    const [serviceList, setServiceList] = useState([]);

    const loadCertificates = async (inputvalue, callback) => {
        const result = await PopulatetypesValue({ type: "PROVIDER_CERTIFICATION" });
        const data = result?.data?.data?.result;

        if (data) {
            callback(
                data?.map((obj) => {
                    return { value: obj?.providerType, label: obj?.providerType?.split("-")[0] };
                })
            );
        }
    };

    // setting certificate
    const handleChangeCat = (option) => {
        setCertificates(option?.map((obj) => obj?.value));
    };

    // setting specilization
    const handleChangeSpecilize = (option) => {
        setSpecilizeList(option?.map((obj) => obj.value));
    };

    //seting services
    const handleChangeService = (option) => {
        setServiceList(option?.map((obj) => obj.value));
    };

    const loadSpecilization = async (inputValue, callback) => {
        const result = await PopulatetypesValue({ type: "PROVIDER_SPECIALIZATION" });

        const data = result?.data?.data?.result;
        if (data) {
            callback(
                data?.map((obj) => {
                    return { value: obj?.providerType, label: obj?.providerType };
                })
            );
        }
    };

    const loadService = async (inputValue, callback) => {
        const result = await PopulatetypesValue({ type: "PROVIDER_SERVICES" });
        const data = result?.data?.data?.result;

        if (data) {
            callback(
                data?.map((obj) => {
                    return { value: obj?.providerType, label: obj?.providerType };
                })
            );
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (certificates?.length === 0 || specilizeList?.length === 0 || serviceList?.length === 0) {
            cogoToast.info("All Field Are mandatory!");
            return;
        }

        const data = new FormData(e.target);

        let obj = {};

        data.forEach((value, key) => (obj[key] = value));

        obj = {
            ...obj,
            certificates: certificates?.map((value) => {
                return {
                    certificate: value,
                };
            }),
            specializations: specilizeList?.map((value) => {
                return { specializationName: value };
            }),
            servives: serviceList?.map((value) => {
                return { serviceName: value };
            }),
        };

        handleStep3Check(obj);
    };

    return (
        <section className="common_section">
            <Form className="formStep1" onSubmit={handleSubmit}>
                {/* Certification */}
                <Form.Group className="mt-4  col-lg-5" controlId="formMultipleSelect">
                    <Form.Label className="title_register">
                        Certification <span className="mandate_field">*</span>
                    </Form.Label>

                    <AsyncSelect cacheOptions defaultOptions loadOptions={loadCertificates} isMulti onChange={handleChangeCat} />
                </Form.Group>

                {/* Specilizations */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Specilizations. <span className="mandate_field">*</span>
                    </Form.Label>

                    <AsyncSelect cacheOptions defaultOptions loadOptions={loadSpecilization} isMulti onChange={handleChangeSpecilize} />
                </Form.Group>

                {/* Services */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">
                        Services. <span className="mandate_field">*</span>
                    </Form.Label>

                    <AsyncSelect cacheOptions defaultOptions loadOptions={loadService} isMulti onChange={handleChangeService} />
                </Form.Group>

                {/* Upload Docs */}
                <Form.Group className="mt-4  col-lg-5">
                    <Form.Label className="title_register">Upload certificates.</Form.Label>

                    <Form.Control
                        // onKeyDown={0}
                        // required
                        name="uploadCertificate"
                        //value={"12345"}
                        type="file"
                        placeholder="upload certificate"
                        // maxLength={25}
                    />
                </Form.Group>

                {/* Submit */}
                <section className="next_button">
                    <button type="submit" className="btn btn-primary submit" style={{ width: "100px" }} disabled={finalSubmit}>
                        {"Submit"}
                    </button>
                </section>
            </Form>
        </section>
    );
};
