export const CHECK_MSG = "OTP Generated. Please check your email and mobile";

export const SERVICE_TYPE = {
    IP: "In-Patient",
    OP: "Out-Patient",
};

export const PREAUTH_STATUS = {
    Requested: "#fdc500",
    Approved: "rgb(0, 139, 60)",
    Cancelled: "#990000",
    Enhancement: "#00008B",
    Rejected: "#FF0000",
    "Document Requested": "#ABB192",
};
