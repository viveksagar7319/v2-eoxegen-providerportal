import { ServiceInstance, SubmitServiceInstance } from "../axiosConfig";

export const getServiceTypeDropdown = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/claimType", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const savePreAuthDetails = async (payLoad) => {
    let result;
    try {
        result = await SubmitServiceInstance.post("/savePreAuthDetails", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const uploadPreauthFiles = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/uploadPreauthDocuemts", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};
