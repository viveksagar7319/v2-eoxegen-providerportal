import { ServiceInstance, SubmitServiceInstance } from "../axiosConfig";

export const Populatelocation = async (payload) => {
    let result;
    try {
        result = await ServiceInstance.post("/populatelocation", payload);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const PopulatetypesValue = async (payload) => {
    let result;
    try {
        result = await ServiceInstance.post("/populatetypesvalue", payload);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const SaveProviderEmpanellment = async (payload) => {
    let result;

    try {
        result = await SubmitServiceInstance.post("/saveproviderempanellment", payload);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};
