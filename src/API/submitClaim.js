import { ServiceInstance, SubmitServiceInstance } from "../axiosConfig";

export const uploadFiles = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/uploadClaimDocuemts", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};
//before
export const getClaimDropDownvalues = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/clauses", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const getCurrencyDropDownvalues = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/currencies", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const getExpenseTypeDropDownvalues = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/serviceclause", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

//Later
export const getClaimTypeDropDownvalues = async (payLoad) => {
    let result;
    try {
        result = await ServiceInstance.post("/claimType", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};

export const saveClaimDetails = async (payLoad) => {
    let result;
    try {
        result = await SubmitServiceInstance.post("/saveClaimDetails", payLoad);
        return { ok: true, data: result, error: null };
    } catch (error) {
        return { ok: false, data: null, error: error };
    }
};
