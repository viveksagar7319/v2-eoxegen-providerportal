import lodash from "lodash";
import deepdash from "deepdash-es";
const _ = deepdash(lodash);

export default _;
