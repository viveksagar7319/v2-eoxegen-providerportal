import React, { useCallback, useEffect, useRef, useState } from "react";
import "./styles.css";
import { Card, Col, Container, Form, Row, Button, Spinner } from "react-bootstrap";
import { useImmer } from "use-immer";
import { ValidateMember } from "../../API/memberEligibility";
import { getServiceTypeDropdown, savePreAuthDetails, uploadPreauthFiles } from "../../API/submitPreAuth";
import cogoToast from "cogo-toast";
// import _ from "lodash";
import { ERROR_CODES, formatDate__ddMMyyyy, getResultFromData, injectID } from "../../utils/utils";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck, faCopy } from "@fortawesome/free-solid-svg-icons";
import { getClaimDropDownvalues } from "../../API/submitClaim";
import Dropzone from "../../Components/Dropzone/Dropzone";
import { fieldValidatePreAuth } from "./validation";
import moment from "moment";
import _ from "../../deepdash";
import AsyncSelect from "react-select/async";
import CustomVirtualList from "../SubmitClaim/VirtualList";
import axios from "axios";
import { decideENV } from "../../decideENV";

const SubmitPreAuth = () => {
    const [formData, setFormData] = useImmer({});
    const [step, setStep] = useState(1);
    const [userDetails] = useState(JSON.parse(sessionStorage.getItem("user")));
    const [timeStamp, setTimeStamp] = useImmer({
        checkin: { hours: "", mins: "" },
        checkout: { hours: "", mins: "" },
    });

    const [successfulDetails, setSuccessfulDetails] = useState(null);

    const proceedToNextStep = (e) => {
        const { id } = e.target;
        const targetStep = id.includes("alt") ? Number(id.slice(id.length - 1)) : Number(id);

        switch (step) {
            case 1: {
                const { error } = fieldValidatePreAuth.validate(
                    _.omit(formData, ["membername", "mobile", "policytodate", "policyfromdate", "relation", "age", "uploadedfiles", "policynumber"]),
                    { abortEarly: false }
                );
                if (error) {
                    cogoToast.error(
                        <ul>
                            {error.details.map((item) => (
                                <li>{item.message}</li>
                            ))}
                        </ul>,
                        {
                            onClick: () => {
                                hide();
                            },
                        }
                    );
                    return;
                } else {
                    setStep(2);
                }

                break;
            }
            case 2: {
                if (targetStep > step) {
                    // setStep(3);

                    SubmitData(formData);
                } else {
                    setStep(targetStep);
                }
                break;
            }
            // case 3: {
            //   if (targetStep > step) {
            //     // setStep(4);
            //     SubmitData(formData);
            //   } else {
            //     setStep(targetStep);
            //   }
            //   break;
            // }
            default: {
                return void 0;
            }
        }
    };

    const updateValues = (field, value) => {
        setFormData((data) => {
            data[field] = value;
        });
    };
    const SubmitData = async (finalData) => {
        const _finalData = _.cloneDeep(finalData);
        const { tokenID, providerName, providerId, userCode } = userDetails;
        const { error } = fieldValidatePreAuth.validate(
            _.omit(_finalData, ["membername", "mobile", "policytodate", "policyfromdate", "relation", "age", "uploadedfiles", "policynumber"])
        );
        if (error) {
            cogoToast.error(error.message);
            return;
        } else {
            const payLoad = {
                preAuth: {
                    membernumber: _finalData.memberno,
                    contactno: _finalData.contact,
                    servicetype: _finalData.servicetype,
                    claimtype: _finalData.claimtype,
                    estimatedamount: _finalData.estimatedcost,
                    checkin: `${formatDate__ddMMyyyy(_finalData.checkin.replaceAll("-", "/"))} ${
                        timeStamp.checkin.hours + ":" + timeStamp.checkin.mins + ":00"
                    }`,
                    checkout: `${formatDate__ddMMyyyy(_finalData.checkout.replaceAll("-", "/"))} ${
                        timeStamp.checkout.hours + ":" + timeStamp.checkout.mins + ":00"
                    }`,
                    tokenID: tokenID,
                    providername: providerName,
                    providerID: providerId,
                    providerCode: userCode,
                    diseaseID: _finalData.icd.value,
                },
            };

            const result = await savePreAuthDetails(payLoad);

            if (result.ok) {
                cogoToast.success("Pre auth submitted successfully");
                if (_finalData.uploadedfiles.files.length > 0) {
                    if (_finalData.uploadedfiles.files.every((file) => file.size > 0)) {
                        const payLoad = {
                            providerID: providerId,
                            tokenID,
                            userID: userCode,
                            claimID: getResultFromData(result).searchValue,
                            file: _finalData.uploadedfiles.files[0],
                        };

                        const _result = await uploadPreauthFiles(payLoad);

                        if (_result.ok) {
                            cogoToast.success("Files Uploaded Successfully");
                        }
                    } else {
                        cogoToast.error("Please upload files with content in it");
                    }
                }

                console.log(getResultFromData(result));

                const transactionID = getResultFromData(result)?.transactionID.split(":")[1];

                if (transactionID) {
                    const detailsToBeShown = {
                        "Preauth Number": transactionID,
                        "Member Name": _finalData.membername,
                        "Member Number": payLoad.preAuth.membernumber,
                        "Contact No": payLoad.preAuth.contactno,
                        "Provider Name": payLoad.preAuth.providername,
                    };
                    setSuccessfulDetails(detailsToBeShown);

                    //save this in state setState(det)
                }

                //Remove all the stepper form and display this transactyion ID
            } else {
                const status = result.error.response.status;
                cogoToast.error(ERROR_CODES[status.toString()] || "Something went wrong");
            }
        }
    };
    useEffect(() => {
        if (successfulDetails) {
            const displayKey = _.omit(successfulDetails, [
                "providerID",
                "providerCode",
                "tokenID",
                "checkin",
                "checkout",
                "servicetype",
                "claimtype",
            ]);

            console.log(displayKey);
        }
    }, [successfulDetails]);

    return (
        <div>
            <Container fluid>
                <Row>
                    <Col md={10}>
                        <h5>Preauth Registration</h5>
                    </Col>
                    {/* <Col md={2} className="text-muted">
            <h6>Dashboard / Submit Preauth</h6>
          </Col> */}
                </Row>
                <Row>
                    <Col md={12}>
                        {successfulDetails === null ? (
                            <Card className="border-0">
                                <Card.Body id="submitpreauthcardbody">
                                    <Card.Title>Request for Preauth</Card.Title>
                                    <Card className="mt-3 col-lg-12 step__flex">
                                        <Button id="1" className={`${step === 1 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                                            1
                                        </Button>
                                        <Button id="2" className={`${step === 2 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                                            2
                                        </Button>
                                        {/* <Button id="3" className={`${step === 3 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                    3
                  </Button> */}
                                    </Card>
                                    <div style={{ display: step === 1 ? "block" : "none" }}>
                                        <Step1
                                            existingData={formData}
                                            updateValues={updateValues}
                                            setTimeStamp={setTimeStamp}
                                            timeStamp={timeStamp}
                                        />
                                    </div>

                                    <div style={{ display: step === 2 ? "block" : "none" }}>
                                        <Step2 existingData={formData} updateValues={updateValues} />
                                    </div>

                                    {/* {step === 1 && <Step1 existingData={formData} updateValues={updateValues} setTimeStamp={setTimeStamp} timeStamp={timeStamp} />} */}
                                    {/* {step === 2 && <Step2 existingData={formData} updateValues={updateValues} />} */}
                                    {/* {step === 3 && <Step3 existingData={formData} updateValues={updateValues} />} */}

                                    <Button
                                        id={`alt_${step + 1}`}
                                        style={{ marginTop: `${step === 2 ? "4rem" : "initial"}` }}
                                        className="mt-3 next_btn"
                                        onClick={proceedToNextStep}
                                    >
                                        {step === 2 ? "Submit" : "Next"}
                                    </Button>
                                </Card.Body>
                            </Card>
                        ) : (
                            <Card>
                                <Card.Body>
                                    {
                                        <div>
                                            <p>
                                                {Object.keys(successfulDetails).map((key, i) => {
                                                    return (
                                                        <p key={i}>
                                                            <span>
                                                                {" "}
                                                                <strong>{key}</strong> :{" "}
                                                            </span>
                                                            <span>{successfulDetails[key]}</span>

                                                            {key === "Preauth Number" ? (
                                                                <span
                                                                    className="icon-conatiner"
                                                                    style={{ marginLeft: "1rem" }}
                                                                    onClick={() =>
                                                                        navigator.clipboard
                                                                            .writeText(successfulDetails[key])
                                                                            .then(() => cogoToast.success("Pre-auth Number copied successfully"))
                                                                    }
                                                                >
                                                                    <FontAwesomeIcon
                                                                        icon={faCopy}
                                                                        style={{
                                                                            cursor: "pointer",
                                                                        }}
                                                                    />
                                                                </span>
                                                            ) : null}
                                                        </p>
                                                    );
                                                })}
                                            </p>
                                        </div>
                                    }
                                </Card.Body>
                            </Card>
                        )}
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default SubmitPreAuth;

const Step1 = ({ existingData, updateValues = () => {}, setTimeStamp = () => {}, timeStamp = {} }) => {
    const [userDetails] = useState(JSON.parse(sessionStorage.getItem("user")));
    const controllerRef = useRef();
    // const modalRef = useRef(null);
    const [, setOTP] = useState("");
    // const OTPref = useRef(null);
    const [, setClaimType] = useState([]);
    const [serviceTypeValues, setServiceTypeValues] = useState([]);
    const [value, setValue] = useState("");
    const firstRender = useRef(true);
    const [isLoading, setIsLoading] = useState({
        spinner: false,
        icon: existingData?.membername?.length > 0 ? true : false,
    });

    const [, setMemberNumber] = useState("");

    // eslint-disable-next-line
    const check = useCallback(
        _.debounce(async (e) => {
            if (e.target.value.length > 0 && e.target.value.length <= 25) {
                const { providerId, tokenID, userCode } = userDetails;

                const payLoad = {
                    memberNo: e.target.value,
                    tokenID: tokenID,
                    userID: userCode,
                    providerID: providerId,
                };
                setIsLoading((val) => ({ ...val, spinner: true, icon: false }));
                const data = await ValidateMember(payLoad);
                if (data.ok) {
                    const result = getResultFromData(data);

                    if (result) {
                        setOTP("");
                        setIsLoading((val) => ({ ...val, icon: true }));
                        setIsLoading((val) => ({ ...val, spinner: false }));

                        const { memberName, policyNo, policyStartDate, policyExpireDate, memberAge, memberRelation } = result;
                        updateValues("memberno", e.target.value);
                        updateValues("membername", memberName);
                        updateValues("policynumber", policyNo);
                        updateValues("policyfromdate", policyStartDate);
                        updateValues("policytodate", policyExpireDate);
                        updateValues("age", memberAge);
                        updateValues("relation", memberRelation);
                    } else {
                        setOTP("");
                        setIsLoading((val) => ({ ...val, spinner: false }));
                        cogoToast.error("Invalid Member Number");
                    }
                } else {
                    setIsLoading(null);
                    cogoToast.error("Something went wrong");
                }
            } else {
                if (e.target.value.length !== 0) cogoToast.info("Membership Number should not be more than 25 characters");
            }

            /** */
        }, 2000),
        []
        // eslint-disable-next-line react-hooks/exhaustive-deps
    );

    const populateServiceType = async (claimTypeValue) => {
        const { tokenID, userCode, providerId } = userDetails;
        const payLoad = {
            userID: userCode,
            tokenID: tokenID,
            providerID: providerId,
            claimType: claimTypeValue,
        };

        const data = await getServiceTypeDropdown(payLoad);
        if (data.ok) {
            setServiceTypeValues(getResultFromData(data));
        } else {
            cogoToast.error("Something went wrong");
        }
        //Bring the results when the api will be done
    };

    const addMins = (time, minsToAdd) => {
        function D(J) {
            return (J < 10 ? "0" : "") + J;
        }
        const piece = time.split(":");
        const mins = piece[0] * 60 + +piece[1] + +minsToAdd;

        return D(((mins % (24 * 60)) / 60) | 0) + ":" + D(mins % 60) + ":" + piece[2];
    };

    useEffect(() => {
        if (existingData.servicetype) {
            updateValues("checkin", moment(new Date()).format("YYYY-MM-DD"));
            updateValues("checkout", moment(new Date()).format("YYYY-MM-DD"));

            if (existingData.servicetype === "IP") {
                setTimeStamp((ts) => {
                    ts.checkin.hours = "00";
                    ts.checkin.mins = "00";
                    ts.checkout.hours = "23";
                    ts.checkout.mins = "59";
                });
            } else if (existingData.servicetype === "OP") {
                setTimeStamp((ts) => {
                    ts.checkin.hours = "09";
                    ts.checkin.mins = "00";
                    ts.checkout.hours = "09";
                    ts.checkout.mins = "10";
                });
            }
        }
    }, [existingData.servicetype]);

    // useEffect(() => {
    //   if (OTP.toString().length === 6) {
    //     handleOTP();
    //   }
    // }, [OTP]);

    useEffect(() => {
        async function getDropDownValues() {
            const { providerId, tokenID, userCode } = userDetails;

            const payLoad = {
                tokenID: tokenID,
                userID: userCode,
                providerID: providerId,
            };
            const resultClaimDropDown = await getClaimDropDownvalues(payLoad);

            if (resultClaimDropDown.ok) {
                setClaimType(getResultFromData(resultClaimDropDown));
            } else {
                cogoToast.error("Something went wrong");
            }
        }
        if (firstRender.current) {
            getDropDownValues();
            firstRender.current = false;
        }
    }, []);

    const handleChange = (change) => {
        updateValues("icd", change);
        setValue(change);
    };

    const debouncedSearch = useCallback(
        _.debounce((value, callBack) => {
            if (controllerRef.current) {
                controllerRef.current.abort();
            }
            controllerRef.current = new AbortController();
            try {
                if (value.length > 0) {
                    const { providerId, tokenID, userCode } = userDetails;

                    const payLoad = {
                        tokenID: tokenID,
                        userID: userCode,
                        providerID: providerId,
                        searchICDorName: value,
                    };
                    axios({
                        url: `${decideENV() === "DEV" ? import.meta.env.VITE_BaseURL_DEV : import.meta.env.VITE_BaseURL_PROD}/diseases`,
                        method: "post",
                        data: payLoad,
                        signal: controllerRef.current.signal,
                        headers: {
                            "eO2-Secret-Code": import.meta.env.VITE_EO2_SECRET_CODE,
                            "Content-Type": "multipart/form-data",
                        },
                    }).then((data) => {
                        if (data.data.result) {
                            callBack(
                                data.data.result.map((item) => ({
                                    value: item.diseaseID,
                                    label: item.diseaseDesc.toUpperCase().slice(0, 30) + "...",
                                }))
                            );
                        } else {
                            cogoToast.error(data.data.message);
                        }
                    });
                }
            } catch (err) {
                console.log(err);
            }
        }, 1000),
        []
        // eslint-disable-next-line react-hooks/exhaustive-deps
    );

    const customStyles = {
        menuList: (provided, state) => ({
            ...provided,
            marginBottom: 10,
        }),
    };

    useEffect(() => {
        if (existingData.servicetype) {
            populateServiceType(existingData.servicetype);
        }
    }, [existingData.servicetype]);

    return (
        <main>
            <Row>
                <Card className="mt-3 border-0">
                    {/* <Card.Title>Preauth Registration</Card.Title> */}
                    <Form className="form">
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Member No. <span className="mandate_field">*</span>
                            <Form.Control
                                onKeyDown={check}
                                required
                                type="text"
                                placeholder="Enter Member No."
                                defaultValue={existingData.memberno ?? ""}
                                maxLength={25}
                                onChange={(e) => {
                                    setMemberNumber(e.target.value);
                                }}
                            />
                            {isLoading?.icon && (
                                <FontAwesomeIcon
                                    icon={faCircleCheck}
                                    style={{
                                        color: "#5cb85c",
                                        position: "relative",
                                        left: "calc(100% - 1.8rem)",
                                        top: "calc(100% - 5.7rem)",
                                    }}
                                />
                            )}{" "}
                            {isLoading?.spinner && (
                                <div
                                    style={{
                                        color: "#556ee6",
                                        position: "relative",
                                        left: "calc(100% - 1.8rem)",
                                        top: "calc(100% - 5.7rem)",
                                        width: "50px",
                                        height: "50px",
                                    }}
                                >
                                    <Spinner animation="border" />
                                </div>
                            )}
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Name
                            <Form.Control disabled value={existingData.membername ?? ""} required type="text" placeholder="Name" />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Policy No.
                            <Form.Control disabled value={existingData.policynumber ?? ""} required type="text" placeholder="Policy Number" />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Age & Relation
                            <Form.Control
                                disabled
                                required
                                value={existingData.age && existingData.relation ? `${existingData.age}, ${existingData.relation}` : ""}
                                type="text"
                                placeholder="Age & Relation"
                            />
                        </Form.Label>
                        {/* <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
              First Enrollment Date
              <Form.Control disabled required type="text" placeholder="Enter First Enrollment Date" />
            </Form.Label> */}
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Policy From Date
                            <Form.Control disabled required value={existingData.policyfromdate ?? ""} type="text" placeholder="Policy From Date" />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Policy To Date
                            <Form.Control disabled required value={existingData.policytodate ?? ""} type="text" placeholder="Policy To Date" />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5">
                            Service Type <span className="mandate_field">*</span>
                            <Form.Select
                                variant="primary"
                                disabled={!isLoading?.icon}
                                value={existingData.servicetype ?? ""}
                                id="servicetype__step1"
                                title="Select Service Type"
                                onChange={(e) => updateValues("servicetype", e.target.value.trim())}
                            >
                                <option disabled value="">
                                    Select
                                </option>
                                {[/* "IP", */ "OP"].map((st) => {
                                    return <option value={st}>{st}</option>;
                                })}
                            </Form.Select>
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5">
                            Claim Type <span className="mandate_field">*</span>
                            <Form.Select
                                variant="primary"
                                disabled={!isLoading?.icon}
                                value={existingData.claimtype ?? ""}
                                id="claimtype__step1"
                                title="Select Claim Type"
                                onChange={(e) => updateValues("claimtype", e.target.value.trim())}
                            >
                                <option disabled value="">
                                    Select
                                </option>
                                {serviceTypeValues?.map((st) => (
                                    <option value={st.serviceCode}>{st.serviceDesc}</option>
                                ))}
                            </Form.Select>
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5">
                            Provider Name <span className="mandate_field">*</span>
                            <Form.Select
                                variant="primary"
                                // disabled={!isLoading?.icon || userDetails?.providerType === "HOSPITAL"}
                                value={existingData.hospitalname ?? ""}
                                id="subclaimtype__step1"
                                title="Select Sub-Claim Type"
                                onChange={(e) => updateValues("hospitalname", e.target.value.trim())}
                            >
                                {!userDetails?.providerType && (
                                    <option disabled value="">
                                        Select
                                    </option>
                                )}
                                {/* TODO : Make this part dynamic */}
                                <option value={userDetails?.providerName} selected={userDetails?.providerType}>
                                    {userDetails?.providerName}
                                </option>
                            </Form.Select>
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Check-In Date <span className="mandate_field">*</span>
                            <div className="timeGroup">
                                <Form.Control
                                    disabled={!isLoading?.icon}
                                    required
                                    value={existingData.checkin ?? ""}
                                    type="date"
                                    placeholder="Enter Date of service"
                                    onChange={(e) => {
                                        updateValues("checkin", e.target.value);
                                        updateValues("checkout", e.target.value);
                                        // if (existingData.servicetype === "OP") {
                                        // }
                                    }}
                                />
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkin.hours}
                                    id="timestampcheckinhrs__step1"
                                    title="Hours"
                                    onChange={(key) => {
                                        setTimeStamp((ts) => {
                                            ts.checkin.hours = key.target.value;
                                        });

                                        if (existingData.servicetype === "OP") {
                                            setTimeStamp((ts) => {
                                                ts.checkout.hours = key.target.value;
                                            });
                                        }
                                    }}
                                >
                                    <option disabled selected value="">
                                        Hours
                                    </option>
                                    {injectID(Array.from({ length: 24 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkin.mins}
                                    id="timestampcheckinmins__step1"
                                    title="Mins"
                                    onChange={(key) => {
                                        setTimeStamp((ts) => {
                                            ts.checkin.mins = key.target.value;
                                        });
                                        if (existingData.servicetype === "OP") {
                                            setTimeStamp((ts) => {
                                                const time = addMins(ts.checkin.hours + ":" + ts.checkin.mins + ":" + "00", 10);
                                                ts.checkout.hours = time.split(":")[0];
                                                ts.checkout.mins = time.split(":")[1];
                                            });
                                        }
                                    }}
                                >
                                    <option disabled selected value="">
                                        Mins
                                    </option>
                                    {injectID(Array.from({ length: 60 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                            </div>
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Check-Out Date <span className="mandate_field">*</span>
                            <div className="timeGroup">
                                <Form.Control
                                    disabled={!isLoading?.icon}
                                    required
                                    value={existingData.checkout ?? ""}
                                    type="date"
                                    min={existingData.checkin}
                                    max={existingData.servicetype === "OP" ? existingData.checkin : null}
                                    placeholder="Enter Expected Date of Discharge"
                                    onChange={(e) => {
                                        updateValues("checkout", e.target.value);
                                        // if (existingData.servicetype !== "OP") {
                                        //   setTimeStamp((ts) => ({
                                        //     ...ts,
                                        //     checkout: new Date().toTimeString().substring(0, 8),
                                        //   }));
                                        // }
                                    }}
                                />

                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkout.hours}
                                    id="timestampcheckouthrs__step1"
                                    title="Hours"
                                    onChange={(key) =>
                                        setTimeStamp((ts) => {
                                            ts.checkout.hours = key.target.value;
                                        })
                                    }
                                >
                                    <option disabled selected value="">
                                        Hours
                                    </option>
                                    {injectID(Array.from({ length: 24 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkout.mins}
                                    id="timestampcheckoutmins__step1"
                                    title="Mins"
                                    onChange={(key) =>
                                        setTimeStamp((ts) => {
                                            ts.checkout.mins = key.target.value;
                                        })
                                    }
                                >
                                    <option disabled selected value="">
                                        Mins
                                    </option>
                                    {injectID(Array.from({ length: 60 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                            </div>
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Estimated Cost <span className="mandate_field">*</span>
                            <Form.Control
                                disabled={!isLoading?.icon}
                                required
                                value={existingData.estimatedcost ?? ""}
                                type="number"
                                placeholder="Enter Estimated Cost"
                                onChange={(e) => updateValues("estimatedcost", e.target.valueAsNumber)}
                            />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Contact Number <span className="mandate_field">*</span>
                            <Form.Control
                                disabled={!isLoading?.icon}
                                required
                                value={existingData.contact ?? ""}
                                type="tel"
                                placeholder="Enter Contact Number"
                                onChange={(e) => updateValues("contact", e.target.value)}
                            />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Mobile Number <span className="mandate_field">*</span>
                            <Form.Control
                                disabled={!isLoading?.icon}
                                required
                                value={existingData.mobile ?? ""}
                                type="tel"
                                placeholder="Enter Mobile Number"
                                onChange={(e) => updateValues("mobile", e.target.value)}
                            />
                        </Form.Label>
                        <Form.Label className="mt-4 col-lg-5" id="async_select" data-isloading={`${!isLoading?.icon}`}>
                            ICD <span className="mandate_field">*</span>
                            <div className="mb-3">
                                <AsyncSelect
                                    menuPlacement="top"
                                    // isMulti
                                    key={`__asyncSelect${Math.random() * 10}`}
                                    cacheOptions
                                    styles={customStyles}
                                    noOptionsMessage={() => "No results to display"}
                                    onChange={handleChange}
                                    value={value}
                                    loadOptions={debouncedSearch}
                                    placeholder="Search ICD"
                                    components={{ MenuList: CustomVirtualList }}
                                />
                            </div>
                        </Form.Label>
                    </Form>
                    {/* <dialog ref={modalRef} className="dialog__modal">
            <section>
              <label htmlFor="otp" />
              Enter OTP
              <input id="otp" name="otp" type="number" value={OTP} className="input" onChange={(e) => setOTP(e.target.valueAsNumber)} />
              <button onClick={handleOTP} ref={OTPref} data-otp="OTP" className="btn btn-primary">
                Submit
              </button>
            </section>
          </dialog> */}
                </Card>
            </Row>
        </main>
    );
};

const Step2 = ({ existingData, updateValues = () => {} }) => {
    const [fileKey, setFileKey] = useState([]);
    const [signal, setSignal] = useState(Math.random());

    const getFileData = (data = []) => {
        if (fileKey.length > 0) {
            updateValues("uploadedfiles", { fileNames: fileKey, files: data });
        } else {
            cogoToast.error("Please Select the type of document");
        }
    };

    useEffect(() => {
        setFileKey([...document.querySelectorAll("[id^=checkbox]")].filter((item) => item.checked).map((item) => item.value));
    }, [signal]);

    useEffect(() => {
        if (fileKey.length === 0) {
            updateValues("uploadedfiles", { fileNames: [], files: [] });
        }
    }, [fileKey]);

    return (
        <Row>
            <Card className="mt-3 border-0">
                <Card.Title>File Upload</Card.Title>
                <Form className="form__submit">
                    Select Documents to Upload
                    <section id="_checkbox_file_selection" style={{ display: "flex", gap: "1rem", marginTop: "1rem" }}>
                        {["Prescription", "Claim Form", "Temporary Invoice"].map((label, index) => (
                            <Form.Label className="d-flex">
                                <Form.Check
                                    className="fileSelectionCheckbox"
                                    onChange={() => setSignal(Math.random())}
                                    value={label?.split(" ")?.join("")?.toLowerCase()}
                                    type="checkbox"
                                    label={label}
                                    key={`__checkbox__${index}`}
                                    id={`checkbox__${index}`}
                                />

                                {/* <span className="mandate_field">*</span> */}
                            </Form.Label>
                        ))}
                    </section>
                    <div
                        style={{
                            pointerEvents: fileKey.length === 0 ? "none" : "initial",
                            opacity: fileKey.length === 0 ? "0.5" : "1",
                        }}
                    >
                        <Form.Label className="mt-4  col-lg-5" style={{ width: "100%", height: "45vh" }}>
                            Attach Document
                            <Dropzone multiple={true} getData={getFileData} />
                        </Form.Label>
                    </div>
                </Form>
            </Card>
        </Row>
    );
};

// const Step3 = ({ existingData, updateValues = () => {} }) => {
//   return <div>Step3</div>;
// };
