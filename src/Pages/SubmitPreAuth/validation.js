import Joi from "joi";

const messages = {
    memberno: "Member No. is required",
    servicetype: "Service Type is required",
    claimtype: "Claim Type is required",
    checkin: "Check In Date is required",
    checkout: "Check Out Date is required",
    estimatedcost: "Estimated Cost is required",
    contact: "Contact is required",
    icd: "ICD is required",
};

export const fieldValidatePreAuth = Joi.object({
    memberno: Joi.string().max(25).required().messages({ "any.required": messages.memberno }),
    servicetype: Joi.string().required().messages({ "any.required": messages.servicetype }),
    claimtype: Joi.string().required().messages({ "any.required": messages.claimtype }),
    checkin: Joi.date().required().messages({ "any.required": messages.checkin }),
    checkout: Joi.date().min(Joi.ref("checkin")).required().messages({ "any.required": messages.checkout }),
    estimatedcost: Joi.number().required().messages({ "any.required": messages.estimatedcost }),
    contact: Joi.string().min(10).max(10).required().messages({ "any.required": messages.contact }),
    icd: Joi.object({ label: Joi.string().required(), value: Joi.string().required() }).required(),
});
