import React, { useCallback, useEffect, useRef, useState } from "react";
import "./styles.css";
import { Container, Row, Col, Form, Button, Card, Spinner, Badge } from "react-bootstrap";

import { formatDate__ddMMyyyy, getErrorResultFromData, getFormValues } from "../../utils/utils";
import { ValidateMember, ValidateMemberWithOTP } from "../../API/memberEligibility";
// import {getICDDetails} from '../../API/submitClaim'
// import _ from "lodash";
import cogoToast from "cogo-toast";
import Dropzone from "../../Components/Dropzone/Dropzone";
import { fieldValidateFirstStep, fieldValidateSecondStep, fieldValidateThirdStep, fieldValidateFourthStep } from "./validations";
import axios from "axios";
import AsyncSelect from "react-select/async";
import CustomVirtualList from "./VirtualList";
import { useImmer } from "use-immer";
import BootstrapTable from "react-bootstrap-table-next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleCheck } from "@fortawesome/free-solid-svg-icons";
import { getResultFromData, injectID } from "../../utils/utils";
import {
    getClaimDropDownvalues,
    getCurrencyDropDownvalues,
    getExpenseTypeDropDownvalues,
    uploadFiles,
    saveClaimDetails,
    getClaimTypeDropDownvalues,
} from "../../API/submitClaim";
// import PreviewData from "./PreviewData";
import { useSnippetStore } from "../../store/store";
import { decideENV } from "../../decideENV";
import moment from "moment";

import _ from "../../deepdash";

const masterProxy = {
    1: {
        memberno: "",
        provider: JSON.parse(sessionStorage.getItem("user")).providerId,
        servicetype: "",
        claimtype: "",
        checkindate: undefined /*Date.now()*/,
        checkoutdate: undefined /*Date.now()*/,
        serviceamount: "",
        icd: [],
        policycode: "",
        membername: "",
        policyperiod: "",
        firstenrollmentdate: "",
        productcurrency: "",
        countrycode: "",
        familycode: "",
        currency: JSON.parse(sessionStorage.getItem("user")).providerCountryCurrency,
    },
    2: {
        0: {
            invoicenumber: "",
            invoicedate: "",
            invoiceamount: "",
            items: [],
        },
    },
    3: undefined,

    4: {
        uploadedFiles: [],
    },
};

const SubmitClaim = () => {
    const [step, setStep] = useState(1);
    const [masterData, setMasterData] = useState(masterProxy);
    const [userDetails] = useState(JSON.parse(sessionStorage.getItem("user")));
    const [timeStamp, setTimeStamp] = useImmer({
        checkindate: { hours: "", mins: "" },
        checkoutdate: { hours: "", mins: "" },
    });
    const { removeSnippet, invoices } = useSnippetStore((state) => state);

    const updateForm = (field, value) => {
        switch (step) {
            case 1: {
                if (field !== "icd") {
                    setMasterData((md) => ({
                        ...md,
                        [step]: { ...md[step], [field]: value },
                    }));
                } else {
                    setMasterData((md) => ({
                        ...md,
                        [step]: {
                            ...md[step],
                            [field]: [
                                ...md[step][field],
                                ...value.map((data) => ({
                                    diseaseICDCode: data.value,
                                    diseaseName: data.label,
                                })),
                            ],
                        },
                    }));
                }
                break;
            }
            case 2: {
                setMasterData((md) => ({ ...md, [step]: field }));
                break;
            }
            case 3: {
                setMasterData((md) => ({ ...md, [step]: field }));
                break;
            }
            case 4: {
                setMasterData((md) => ({
                    ...md,
                    [step]: { ...md[step], [field]: value },
                }));
                break;
            }
            default: {
                console.log("Invalid step");
            }
        }
    };

    const SubmitData = async (finalData) => {
        const { providerId, tokenID, userCode, providerName } = userDetails;
        const _finalData = _.cloneDeep(finalData);
        const payLoad = {
            claims: {
                membernumber: _finalData[1].memberno,
                countrycode: _finalData[1].countrycode.trim(),
                policycurrencycode: _finalData[1].productcurrency.trim(),
                claimamount: _finalData[1].serviceamount,
                patientname: _finalData[1].membername,
                checkin: `${formatDate__ddMMyyyy(_finalData[1].checkindate.replaceAll("-", "/"))} ${
                    timeStamp.checkindate.hours + ":" + timeStamp.checkindate.mins + ":00"
                }`,
                checkout: `${formatDate__ddMMyyyy(_finalData[1].checkoutdate.replaceAll("-", "/"))} ${
                    timeStamp.checkoutdate.hours + ":" + timeStamp.checkoutdate.mins + ":00"
                }`,
                familycode: _finalData[1].familycode,
                currency: _finalData[1].currency,
                benefitcode: _finalData[1].claimtype.trim(),
                servicetype: _finalData[1].servicetype,
                tokenID: tokenID,
                providername: providerName,
                providerID: providerId,
                providerCode: userCode,
                diseases: _.uniqBy(_finalData[1].icd, "diseaseICDCode"),
                invoices: _.omitDeep(invoices, ["id"]) /*_finalData[3]*/,
            },
        };

        //bulk validate rather here

        const saveResult = await saveClaimDetails(payLoad);

        if (!saveResult.ok) {
            cogoToast.error(getErrorResultFromData(saveResult).message);
        } else {
            cogoToast.success("Claim submitted successfully");
            removeSnippet();
        }

        if (_finalData[4].uploadedFiles.length > 0) {
            if (saveResult.ok) {
                const apiResponse = getResultFromData(saveResult);
                const payLoad = {
                    tokenID: tokenID,
                    providerID: providerId,
                    userID: userCode,
                    file: _finalData[4].uploadedFiles[0],
                    barCode: apiResponse?.transactionID?.split(":")[1],
                };
                const result = await uploadFiles(payLoad);
                if (result.ok) {
                    cogoToast.success("Files uploaded successfully");
                } else {
                    cogoToast.error(getErrorResultFromData(result).message);
                }
            }
        }
    };

    const isValidFirstStep = () => {
        const { error } = fieldValidateFirstStep.validate(masterData[1], { abortEarly: false });
        if (error) {
            cogoToast.error(
                <ul>
                    {error.details.map((item) => (
                        <li>{item.message}</li>
                    ))}
                </ul>
            );
            return false;
        } else {
            return true;
        }
    };

    const isValidSecondStep = () => {
        const { error } = fieldValidateSecondStep.validate(masterData[2]);
        if (error) {
            cogoToast.error(error.message);
            return false;
        } else {
            if (Object.values(masterData[2]).reduce((acc, item) => acc + item.invoiceamount ?? 0, 0) === masterData[1].serviceamount) {
                return true;
            } else {
                cogoToast.error("Collective invoice amount should equal claim amount.Please edit or delete fields");
                return false;
            }
        }
    };

    const isValidThirdStep = () => {
        const { error } = fieldValidateThirdStep.validate(masterData[3]);
        if (error) {
            cogoToast.error(error.message);
            return false;
        } else {
            if (invoices.length > 0 && invoices.every((data) => data.invoiceamount === data.items.reduce((acc, item) => item.amount + acc, 0))) {
                return true;
            } else {
                cogoToast.error("Line Items amount should add upto total invoice amount");
                return false;
            }
        }
    };

    const isValidFourthStep = () => {
        const { error } = fieldValidateFourthStep.validate(masterData[4]);
        if (error) {
            cogoToast.error(error.message);
            return false;
        } else {
            return true;
        }
    };

    const proceedToNextStep = (e) => {
        const { id } = e.target;
        const targetStep = id.includes("alt") ? Number(id.slice(id.length - 1)) : Number(id);

        switch (step) {
            case 1: {
                if (targetStep > step && targetStep - step === 1) {
                    if (isValidFirstStep()) {
                        setStep(2);
                    }
                } else {
                    if (targetStep - step === 3) {
                        if (isValidFirstStep()) {
                            if (isValidSecondStep()) {
                                if (isValidThirdStep()) {
                                    setStep(targetStep);
                                }
                            }
                        }
                    } else if (targetStep - step === 2) {
                        if (isValidFirstStep()) {
                            if (isValidSecondStep()) {
                                setStep(targetStep);
                            }
                        }
                    }
                }

                break;
            }
            case 2: {
                if (targetStep > step && targetStep - step === 1) {
                    if (isValidSecondStep()) {
                        setStep(3);
                    }
                } else {
                    if (targetStep - step === 2) {
                        if (isValidSecondStep()) {
                            if (isValidThirdStep()) {
                                setStep(targetStep);
                            }
                        }
                    } else if (targetStep - step < 0) {
                        setStep(targetStep);
                    }
                }

                break;
            }
            case 3: {
                if (targetStep > step && targetStep - step === 1) {
                    if (isValidThirdStep()) {
                        setStep(4);
                    }
                } else {
                    setStep(targetStep);
                }

                break;
            }
            case 4: {
                if (targetStep > step) {
                    if (isValidFourthStep()) {
                        SubmitData(masterData);
                    }
                } else {
                    setStep(targetStep);
                }
                break;
            }
            default: {
                return void 0;
            }
        }
    };

    return (
        <div>
            <Container fluid>
                <Row>
                    <Col md={10}>
                        <h5>Claim Registration</h5>
                    </Col>
                    {/* <Col md={2} className="text-muted">
            <h6>Dashboard / Submit Claim</h6>
          </Col> */}
                </Row>
                <Row>
                    <Col md={12}>
                        <Card className="border-0">
                            <Card.Body>
                                <Card.Title>Request for Cashless</Card.Title>
                                <Card className="mt-3 col-lg-12 step__flex">
                                    <Button id="1" className={`${step === 1 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                                        1
                                    </Button>
                                    <Button id="2" className={`${step === 2 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                                        2
                                    </Button>
                                    <Button id="3" className={`${step === 3 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                                        3
                                    </Button>
                                    <Button id="4" className={`${step === 4 ? "step_actve" : "steps"}`} onClick={proceedToNextStep}>
                                        4
                                    </Button>
                                </Card>
                                <div style={{ display: step === 1 ? "block" : "none" }}>
                                    <Step1 update={updateForm} existingData={masterData[1]} setTimeStamp={setTimeStamp} timeStamp={timeStamp} />
                                </div>
                                <div style={{ display: step === 2 ? "block" : "none" }}>
                                    <Step2
                                        update={updateForm}
                                        existingData={masterData[2]}
                                        appendField={setMasterData}
                                        setTimeStamp={setTimeStamp}
                                        claimAmount={masterData[1].serviceamount}
                                    />
                                </div>
                                <div style={{ display: step === 3 ? "block" : "none" }}>
                                    <Step3 update={updateForm} existingData={masterData[3]} invoiceData={masterData[2]} />
                                </div>
                                <div style={{ display: step === 4 ? "block" : "none" }}>
                                    <Step4 update={updateForm} />
                                </div>

                                <Button id={`alt_${step + 1}`} className="mt-3 next_btn" onClick={proceedToNextStep}>
                                    {step === 4 ? "Submit" : "Next"}
                                </Button>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

const Step1 = ({ update: sendData = () => {}, existingData, setTimeStamp = () => {}, timeStamp = {} }) => {
    const [userDetails] = useState(JSON.parse(sessionStorage.getItem("user")));
    const controllerRef = useRef();
    const [value, setValue] = useState("");
    const [isLoading, setIsLoading] = useState({
        spinner: false,
        icon: existingData?.membername?.length > 0 ? true : false,
    });
    const firstRender = useRef(true);
    const modalRef = useRef(null);
    const [claimType, setClaimType] = useState([]);
    const [currencyType, setCurrencyType] = useState([]);
    const [OTP, setOTP] = useState("");

    // eslint-disable-next-line
    const debouncedSearch = useCallback(
        _.debounce((value, callBack) => {
            if (controllerRef.current) {
                controllerRef.current.abort();
            }
            controllerRef.current = new AbortController();
            try {
                if (value.length > 0) {
                    const { providerId, tokenID, userCode } = userDetails;

                    const payLoad = {
                        tokenID: tokenID,
                        userID: userCode,
                        providerID: providerId,
                        searchICDorName: value,
                    };
                    axios({
                        url: `${decideENV() === "DEV" ? import.meta.env.VITE_BaseURL_DEV : import.meta.env.VITE_BaseURL_PROD}/diseases`,
                        method: "post",
                        data: payLoad,
                        signal: controllerRef.current.signal,
                        headers: {
                            "eO2-Secret-Code": import.meta.env.VITE_EO2_SECRET_CODE,
                            "Content-Type": "multipart/form-data",
                        },
                    }).then((data) => {
                        if (data.data.result) {
                            callBack(
                                data.data.result.map((item) => ({
                                    value: item.diseaseICDCode,
                                    label: item.diseaseDesc.toUpperCase().slice(0, 30) + "...",
                                }))
                            );
                        } else {
                            cogoToast.error(data.data.message);
                        }
                    });
                }
            } catch (err) {
                console.log(err);
            }
        }, 1000),
        []
        // eslint-disable-next-line react-hooks/exhaustive-deps
    );

    const handleChange = (change) => {
        sendData("icd", change);
        setValue(change);
    };

    const check = useCallback(
        _.debounce(async (e) => {
            /**Will have to make two api calls */
            if (e.target.value.length > 0 && e.target.value.length <= 25) {
                const { providerId, tokenID, userCode } = userDetails;

                const payLoad = {
                    memberNo: e.target.value,
                    tokenID: tokenID,
                    userID: userCode,
                    providerID: providerId,
                };
                setIsLoading((val) => ({ ...val, spinner: true, icon: false }));
                const data = await ValidateMember(payLoad);
                if (data.ok) {
                    const result = getResultFromData(data);
                    if (result) {
                        const { policyNo, memberName, policyStartDate, policyExpireDate, productCurrency, countryCode, empCode } = result;
                        sendData("policycode", policyNo);
                        sendData("membername", memberName);
                        sendData("policyperiod", policyStartDate.replaceAll("-", "/") + " - " + policyExpireDate.replaceAll("-", "/"));
                        sendData("firstenrollmentdate", policyStartDate);
                        sendData("productcurrency", productCurrency);
                        sendData("countrycode", countryCode);
                        sendData("familycode", empCode);
                        setIsLoading((val) => ({ ...val, spinner: false }));
                        setIsLoading((val) => ({ ...val, icon: true }));
                    } else {
                        cogoToast.error("Invalid Policy Number");
                        setIsLoading((val) => ({ ...val, spinner: false, icon: false }));
                        // setIsLoading((val) => ({ ...val, icon: true }));
                    }
                } else {
                    // setIsLoading(null);
                    // cogoToast.error("Something went wrong");

                    setOTP("");
                    setIsLoading((val) => ({ ...val, spinner: false }));
                    setIsLoading((val) => ({ ...val, icon: false }));
                    cogoToast.error("Something went wrong");
                    sendData("policycode", "");
                    sendData("policyperiod", ``);
                    sendData("membername", "");
                    sendData("firstenrollmentdate", "");
                }
            } else {
                if (e.target.value.length !== 0) cogoToast.info("Membership Number should not be more than 25 characters");
            }

            /** */
        }, 2000),
        []
        // eslint-disable-next-line react-hooks/exhaustive-deps
    );

    const customStyles = {
        menuList: (provided, state) => ({
            ...provided,
            marginBottom: 10,
        }),
    };

    const addMins = (time, minsToAdd) => {
        function D(J) {
            return (J < 10 ? "0" : "") + J;
        }
        const piece = time.split(":");
        const mins = piece[0] * 60 + +piece[1] + +minsToAdd;

        return D(((mins % (24 * 60)) / 60) | 0) + ":" + D(mins % 60) + ":" + piece[2];
    };

    useEffect(() => {
        if (existingData.servicetype) {
            sendData("checkindate", moment(new Date()).format("YYYY-MM-DD"));
            sendData("checkoutdate", moment(new Date()).format("YYYY-MM-DD"));

            if (existingData.servicetype === "IP") {
                setTimeStamp((ts) => {
                    ts.checkindate.hours = "09";
                    ts.checkindate.mins = "00";
                    ts.checkoutdate.hours = "23";
                    ts.checkoutdate.mins = "59";
                });
            } else if (existingData.servicetype === "OP") {
                setTimeStamp((ts) => {
                    ts.checkindate.hours = "09";
                    ts.checkindate.mins = "00";
                    ts.checkoutdate.hours = "09";
                    ts.checkoutdate.mins = "10";
                });
            }
        }
    }, [existingData.servicetype]);

    const getClaimTypeDropDownValues = async (claimTypeValue) => {
        const { providerId, tokenID, userCode } = userDetails;

        const payLoad = {
            tokenID: tokenID,
            userID: userCode,
            providerID: providerId,
            claimType: claimTypeValue,
        };

        // const data = await getClaimTypeDropDownvalues(payLoad);
        const data = await getClaimDropDownvalues(payLoad);

        if (data.ok) {
            setClaimType(getResultFromData(data));
        } else {
            cogoToast.error("Something went wrong");
        }
    };

    useEffect(() => {
        if (existingData.servicetype) {
            getClaimTypeDropDownValues(existingData.servicetype);
        }
    }, [existingData.servicetype]);

    useEffect(() => {
        async function getDropDownValues() {
            const { providerId, tokenID, userCode } = userDetails;

            const payLoad = {
                tokenID: tokenID,
                userID: userCode,
                providerID: providerId,
            };
            // const [resultClaimDropDown, resultCurrencyDropDown] = await Promise.all([
            //   getExpenseTypeDropDownvalues(payLoad),
            //   getCurrencyDropDownvalues(payLoad),
            // ]);
            const resultCurrencyDropDown = await getCurrencyDropDownvalues(payLoad);

            // if (resultClaimDropDown.ok) {
            //   setClaimType(getResultFromData(resultClaimDropDown));
            // } else {
            //   cogoToast.error("Something went wrong");
            // }
            if (resultCurrencyDropDown.ok) {
                setCurrencyType(getResultFromData(resultCurrencyDropDown));
            }
        }
        if (firstRender.current) {
            getDropDownValues();
            firstRender.current = false;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <main>
            <Row>
                <Card className="mt-3 border-0">
                    {/* <Card.Title>Claim Registration</Card.Title> */}
                    <Form className="form__el" style={{ columnGap: "10rem" }}>
                        <Form.Label className="mt-4  col-lg-5" style={{ maxHeight: "62px" }}>
                            Member No. <span className="mandate_field">*</span>
                            <Form.Control
                                onKeyDown={check}
                                required
                                value={existingData.memberno.toUpperCase()}
                                type="text"
                                placeholder="Enter Member No."
                                maxLength={25}
                                onChange={(key) => sendData("memberno", key.target.value)}
                            />
                            {isLoading?.icon && (
                                <FontAwesomeIcon
                                    icon={faCircleCheck}
                                    style={{
                                        color: "#5cb85c",
                                        position: "relative",
                                        left: "calc(100% - 1.8rem)",
                                        top: "calc(100% - 5.7rem)",
                                    }}
                                />
                            )}{" "}
                            {isLoading?.spinner && (
                                <div
                                    style={{
                                        color: "#556ee6",
                                        position: "relative",
                                        left: "calc(100% - 1.8rem)",
                                        top: "calc(100% - 5.7rem)",
                                        width: "50px",
                                        height: "50px",
                                    }}
                                >
                                    <Spinner animation="border" />
                                </div>
                            )}
                        </Form.Label>{" "}
                        {/* <dialog ref={modalRef} className="dialog__modal">
              <section>
                <label htmlFor="otp" />
                Enter OTP
                <input
                  id="otp"
                  name="otp"
                  type="number"
                  onWheel={(e) => e.preventDefault()}
                  value={OTP}
                  className="input"
                  onChange={(e) => setOTP(e.target.valueAsNumber)}
                />
                <button onClick={handleOTP} data-otp="OTP" className="btn btn-primary">
                  Submit
                </button>
              </section>
            </dialog> */}
                        <Form.Label className="mt-4  col-lg-5">
                            Policy Code
                            <Form.Control type="text" disabled value={existingData.policycode} placeholder="Policy Code" />
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Member Name
                            <Form.Control type="text" disabled value={existingData.membername} placeholder="Member Name" />
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Policy Period
                            <Form.Control type="text" disabled value={existingData.policyperiod} placeholder="Policy Period" />
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            First Enrollment Date
                            <Form.Control type="text" disabled value={existingData.firstenrollmentdate} placeholder="First Enrollment Date" />
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Service Type <span className="mandate_field">*</span>
                            <Form.Select
                                defaultValue=""
                                variant="primary"
                                disabled={!isLoading?.icon}
                                value={existingData.servicetype}
                                id="service__step1"
                                title="Select Service Type"
                                onChange={(key) => sendData("servicetype", key.target.value)}
                            >
                                <option disabled value="">
                                    Select
                                </option>
                                <option value="IP">IP</option>
                                <option value="OP">OP</option>
                            </Form.Select>
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Claim Type <span className="mandate_field">*</span>
                            <Form.Select
                                variant="primary"
                                disabled={!isLoading?.icon}
                                value={existingData.claimtype ?? " "}
                                id="claimtype__step1"
                                title="Select Claim Type"
                                onChange={(key) => sendData("claimtype", key.target.value)}
                            >
                                <option disabled value="">
                                    Select
                                </option>
                                {/* {injectID(claimType)?.map((ct) => (
                  <option key={ct.id} value={ct.data.clauseCode}>
                    {ct.data.clauseDesc}
                  </option>
                ))} */}
                                {/* {injectID(claimType)?.map((ct) => (
                  <option key={ct.id} value={ct.serviceCode}>
                    {ct.serviceDesc}
                  </option>
                ))} */}

                                {/* {claimType?.map((ct) => (
                                    <option key={ct.id} value={ct.serviceCode}>
                                        {ct.serviceDesc}
                                    </option>
                                ))} */}

                                {claimType?.map((ct) => (
                                    <option key={ct.id} value={ct.clauseCode}>
                                        {ct.clauseDesc}
                                    </option>
                                ))}
                            </Form.Select>
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Check-In Date <span className="mandate_field">*</span>
                            <div className="timeGroup">
                                <Form.Control
                                    type="date"
                                    disabled={!isLoading?.icon}
                                    value={existingData.checkindate || moment(new Date()).format("YYYY-MM-DD")}
                                    onChange={(e) => {
                                        if (existingData.servicetype.length > 0) {
                                            sendData("checkindate", e.target.value);

                                            if (existingData.servicetype === "OP") {
                                                sendData("checkoutdate", e.target.value);
                                            }
                                        } else {
                                            cogoToast.error("Please select service type first");
                                        }
                                    }}
                                />
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkindate.hours}
                                    id="timestampcheckinhrs__step1"
                                    title="Hours"
                                    onChange={(key) => {
                                        setTimeStamp((ts) => {
                                            ts.checkindate.hours = key.target.value;
                                        });

                                        if (existingData.servicetype === "OP") {
                                            setTimeStamp((ts) => {
                                                ts.checkoutdate.hours = key.target.value;
                                            });
                                        }
                                    }}
                                >
                                    <option disabled selected value="">
                                        Hours
                                    </option>
                                    {injectID(Array.from({ length: 24 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkindate.mins}
                                    id="timestampcheckinmins__step1"
                                    title="Mins"
                                    onChange={(key) => {
                                        setTimeStamp((ts) => {
                                            ts.checkindate.mins = key.target.value;
                                        });
                                        if (existingData.servicetype === "OP") {
                                            setTimeStamp((ts) => {
                                                const time = addMins(ts.checkindate.hours + ":" + ts.checkindate.mins + ":" + "00", 10);
                                                ts.checkoutdate.hours = time.split(":")[0];
                                                ts.checkoutdate.mins = time.split(":")[1];
                                            });
                                        }
                                    }}
                                >
                                    <option disabled selected value="">
                                        Mins
                                    </option>
                                    {injectID(Array.from({ length: 60 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                            </div>
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Check-Out Date <span className="mandate_field">*</span>
                            <div className="timeGroup">
                                <Form.Control
                                    type="date"
                                    disabled={!isLoading?.icon}
                                    value={existingData.checkoutdate || moment(new Date()).format("YYYY-MM-DD")}
                                    onChange={(e) => {
                                        if (existingData.servicetype.length > 0) {
                                            sendData("checkoutdate", e.target.value);
                                        } else {
                                            cogoToast.error("Please select service type first");
                                        }
                                    }}
                                />{" "}
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkoutdate.hours}
                                    id="timestampcheckouthrs__step1"
                                    title="Hours"
                                    onChange={(key) =>
                                        setTimeStamp((ts) => {
                                            ts.checkoutdate.hours = key.target.value;
                                        })
                                    }
                                >
                                    <option disabled selected value="">
                                        Hours
                                    </option>
                                    {injectID(Array.from({ length: 24 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                                <Form.Select
                                    variant="primary"
                                    disabled={!isLoading?.icon}
                                    value={timeStamp.checkoutdate.mins}
                                    id="timestampcheckoutmins__step1"
                                    title="Mins"
                                    onChange={(key) =>
                                        setTimeStamp((ts) => {
                                            ts.checkoutdate.mins = key.target.value;
                                        })
                                    }
                                >
                                    <option disabled selected value="">
                                        Mins
                                    </option>
                                    {injectID(Array.from({ length: 60 }, (_, index) => (index < 10 ? "0" + index : index))).map((hr) => (
                                        <option key={hr.id} value={hr.data}>
                                            {hr.data}
                                        </option>
                                    ))}
                                </Form.Select>
                            </div>
                        </Form.Label>{" "}
                        <Form.Label className="mt-4  col-lg-5">
                            Service Amount <span className="mandate_field">*</span>
                            <Form.Control
                                type="number"
                                onWheel={(e) => console.log(e)}
                                disabled={!isLoading?.icon}
                                value={existingData.serviceamount}
                                placeholder="Enter Service Amt."
                                onChange={(e) => sendData("serviceamount", e.target.valueAsNumber)}
                            />
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5">
                            Currency
                            <Form.Select
                                variant="primary"
                                disabled={!isLoading?.icon}
                                value={existingData?.currency || ""}
                                id="currency__step1"
                                title="Select Currency"
                                onChange={(key) => sendData("currency", key.target.value)}
                            >
                                <option disabled value="">
                                    Select
                                </option>
                                {currencyType?.map((ct) => (
                                    <option value={ct.currencyCode}>{ct.currencyName}</option>
                                ))}
                            </Form.Select>
                        </Form.Label>
                        <Form.Label className="mt-4  col-lg-5" id="async_select" data-isloading={`${!isLoading?.icon}`}>
                            ICD <span className="mandate_field">*</span>
                            <div className="mb-3">
                                <AsyncSelect
                                    menuPlacement="top"
                                    isMulti
                                    key={`__asyncSelect${Math.random() * 10}`}
                                    cacheOptions
                                    styles={customStyles}
                                    noOptionsMessage={() => "No results to display"}
                                    onChange={handleChange}
                                    value={value}
                                    loadOptions={debouncedSearch}
                                    placeholder="Search ICD"
                                    components={{ MenuList: CustomVirtualList }}
                                />
                            </div>
                        </Form.Label>
                    </Form>
                </Card>
            </Row>
        </main>
    );
};

const Step2 = ({ update: sendData = () => {}, existingData, setTimeStamp = () => {}, claimAmount }) => {
    const [data, setData] = useImmer({
        0: {
            invoicenumber: "",
            invoicedate: "",
            invoiceamount: "",
            items: [],
        },
    });
    const firstRender = useRef(true);
    const [id, setId] = useState(0);
    const [tableData, setTableData] = useState([]);
    const [tableHeaders, setTableHeaders] = useState([]);
    const [currentEditableRow, setCurrentEditableRow] = useState();
    const modalRef = useRef(null);
    const existingDataCopy = useRef(null);

    const saveValues = (field, value, id) => {
        setData((data) => {
            if (data[id]) {
                data[id][field] = value;
                data[id].items = []; //Important
            } else {
                data[id] = {};
                data[id][field] = value;
                data[id].items = []; //Important
            }
        });
        firstRender.current = false;
    };

    const add = () => {
        if (data[id]) {
            const { invoicenumber, invoicedate, invoiceamount } = data[id];
            if (typeof invoicenumber === "string" && invoicedate?.length > 0 && typeof invoiceamount === "number") {
                if (!Object.values(tableData).some((data) => data.invoicenumber === invoicenumber)) {
                    sendData(data);
                    setId((id) => ++id);
                } else {
                    cogoToast.error("Invoice Number should be unique");
                }
            } else {
                cogoToast.error("Fill all the fields");
            }
        } else {
            cogoToast.error("Start entering values in all the fields");
        }
    };

    const handleDelete = (row) => {
        let replacedResults = {};
        Object.values(_.cloneDeep(existingDataCopy.current))
            .filter((item) => item.invoicenumber !== row.invoicenumber)
            .flat()
            .forEach((item, key) => {
                replacedResults[key] = item;
            });
        sendData(replacedResults);
        setData(replacedResults);

        if (id > 0) {
            setId((id) => --id);
        }
    };

    const handleEdit = (e) => {
        e.preventDefault();
        e.persist();
        const formValues = getFormValues(e.target.elements, "invoicenumber", "invoicedate", "invoiceamount");

        if (!_.isEmpty(formValues)) {
            formValues.invoicenumber = formValues.invoicenumber === "" ? currentEditableRow.invoicenumber : formValues.invoicenumber;
            formValues.invoiceamount = formValues.invoiceamount === "" ? currentEditableRow.invoiceamount : Number(formValues.invoiceamount);
            formValues.invoicedate =
                formValues.invoicedate === ""
                    ? currentEditableRow.invoicedate
                    : `${formatDate__ddMMyyyy(formValues.invoicedate)} ${new Date().toTimeString().substring(0, 8)}`;

            setData((data) => {
                if (data.invoicenumber === currentEditableRow.invoiceamount) {
                    data.invoicenumber = formValues.invoicenumber;
                    data.invoiceamount = formValues.invoiceamount;
                    data.invoicedate = formValues.invoicedate;
                }
            });

            const _existingData = _.cloneDeep(existingData);

            for (let [key, value] of Object.entries(_existingData)) {
                if (value.invoicenumber === currentEditableRow.invoicenumber) {
                    _existingData[key].invoicenumber = formValues.invoicenumber;
                    _existingData[key].invoiceamount = formValues.invoiceamount;
                    _existingData[key].invoicedate = formValues.invoicedate;
                }
            }

            sendData(_existingData);
        }
        document.getElementById("Step2EditForm").reset();
    };

    const generateTable = (tableData) => {
        const tableHeaderContext = [
            {
                dataField: "invoicenumber",
                text: "Invoice No.",
                sort: false,
            },
            {
                dataField: "invoicedate",
                text: "Invoice Date",
                sort: false,
            },
            {
                dataField: "invoiceamount",
                text: "Invoice Amount",
                sort: false,
            },
            {
                dataField: "edit",
                text: "Edit",
                sort: false,
                formatter: (cellContent, row) => {
                    return (
                        <Button
                            style={{ width: "max-content" }}
                            onClick={() => {
                                setCurrentEditableRow(row);
                                modalRef.current.showModal();
                            }}
                        >
                            Edit
                        </Button>
                    );
                },
            },
            {
                dataField: "delete",
                text: "Delete",
                sort: false,
                formatter: (cellContent, row) => {
                    return (
                        <Button style={{ width: "max-content" }} variant="danger" onClick={() => handleDelete(row)}>
                            Delete
                        </Button>
                    );
                },
            },
        ];
        setTableHeaders(tableHeaderContext);
        const testObject = Object.keys(tableData);
        if (
            testObject?.length === 1 &&
            testObject[0] === "0" &&
            Object.values(tableData[0]).every((data) => data === "" || (Array.isArray(data) && data.length === 0))
        ) {
            void 0;
        } else {
            setTableData(Object.values(tableData));
        }
    };

    useEffect(() => {
        if (existingData) {
            existingDataCopy.current = existingData;
            generateTable(existingData);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [existingData]);

    useEffect(() => {
        setData(existingData);
        // eslint-disable-next-line react-hooks/exhaustive-deps
        return () => {
            setData({
                0: {
                    invoicenumber: "",
                    invoicedate: "",
                    invoiceamount: "",
                    items: [],
                },
            });
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <main>
            <Row>
                <Card className="mt-3 border-0">
                    <Card.Title>Invoice Details</Card.Title>
                    <InvoiceDetails
                        key={`invoice_${id}`}
                        existingData={data[id]}
                        sendData={saveValues}
                        id={id}
                        add={add}
                        setTimeStamp={setTimeStamp}
                    />
                    {tableData.length > 0 && (
                        <BootstrapTable bootstrap4={true} keyField="invoicenumber" data={tableData} columns={tableHeaders} bordered={false} />
                    )}

                    {currentEditableRow?.invoicenumber && currentEditableRow?.invoicedate && currentEditableRow?.invoiceamount && (
                        <dialog ref={modalRef} method="dialog" onSubmit={handleEdit}>
                            <Form className="form__el" id="Step2EditForm">
                                <Form.Label className="mt-4  col-md-3">
                                    Invoice No.
                                    <Form.Control
                                        type="text"
                                        defaultValue={currentEditableRow?.invoicenumber || ""}
                                        onWheel={(e) => e.preventDefault()}
                                        name="invoicenumber"
                                        placeholder="Enter Invoice No."
                                    />
                                </Form.Label>{" "}
                                <Form.Label className="mt-4  col-md-3">
                                    Invoice Date
                                    <Form.Control
                                        type="date"
                                        defaultValue={moment(new Date(currentEditableRow?.invoicedate)).format("YYYY-MM-DD") || ""}
                                        name="invoicedate"
                                        placeholder="Enter Invoice Date."
                                    />
                                </Form.Label>{" "}
                                <Form.Label className="mt-4  col-md-3">
                                    Invoice Amount
                                    <Form.Control
                                        type="number"
                                        defaultValue={currentEditableRow?.invoiceamount || 0}
                                        onWheel={(e) => e.preventDefault()}
                                        name="invoiceamount"
                                        placeholder="Enter Invoice "
                                    />
                                </Form.Label>{" "}
                                <Button
                                    className="mt-5 next_btn"
                                    style={{ marginLeft: "unset" }}
                                    type="submit"
                                    onClick={() => {
                                        modalRef.current.close();
                                    }}
                                >
                                    Save
                                </Button>
                            </Form>
                        </dialog>
                    )}
                </Card>
            </Row>
        </main>
    );
};

const Step3 = ({ update: sendData = () => {}, existingData, invoiceData }) => {
    const [renderSnippet, setRenderSnippet] = useImmer();
    const { setInvoice, removeSnippet } = useSnippetStore((state) => state);
    useEffect(() => {
        if (Object.values(invoiceData).every((item) => (item.invoicenumber !== "") & (item.invoicedate !== "") && item.invoiceamount !== "")) {
            setInvoice("invoices", Object.values(invoiceData));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [invoiceData]);

    useEffect(() => {
        setRenderSnippet(
            Object.values(invoiceData).reduce(
                (acc, item) => ({
                    ...acc,
                    [item.invoicenumber]: [
                        <InvoiceFormSnippet
                            id={`${item.invoicenumber}_${crypto?.randomUUID?.() || Math.random().toString(36).slice(2)}`}
                            stepData={item}
                            remove={remove}
                        />,
                    ],
                }),
                {}
            )
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [invoiceData]);

    const remove = (id, invoice) => {
        setRenderSnippet((rs) => {
            if (rs?.[id.split("_")[0]]?.length > 1) {
                rs[id.split("_")[0]] = rs[id.split("_")[0]].filter((Item) => {
                    return Item.props.id !== id;
                });
            } else {
                console.log(rs[id[0]]);
            }
        });
        //remove the data;
        removeSnippet(id);
    };
    return (
        <main>
            {" "}
            <Row>
                <Card className="mt-3 border-0">
                    <Card.Title>Items</Card.Title>
                    {Object.values(invoiceData).map((item, key) => (
                        <>
                            <Badge bg="primary" style={{ width: "fit-content", fontSize: "1.25rem" }}>
                                Invoice Number: {item.invoicenumber}
                            </Badge>
                            {Array.isArray(renderSnippet?.[item.invoicenumber]) &&
                                renderSnippet?.[item.invoicenumber]?.map((Snippet, key) => Snippet)}
                            <Button
                                className="snippetButton"
                                onClick={() =>
                                    setRenderSnippet((val) => {
                                        val[item.invoicenumber].push(
                                            <InvoiceFormSnippet
                                                id={`${item.invoicenumber}_${
                                                    crypto?.randomUUID?.() || Math.random().toString(36).slice(2) /*val[item.invoicenumber].length*/
                                                }`}
                                                stepData={item}
                                                remove={remove}
                                            />
                                        );
                                    })
                                }
                            >
                                +
                            </Button>
                        </>
                    ))}
                </Card>
            </Row>
        </main>
    );
};

const Step4 = ({ update: sendData = () => {} }) => {
    const getFileData = (data = []) => {
        sendData("uploadedFiles", data);
    };
    return (
        <Row>
            <Card className="mt-3 border-0">
                <Card.Title>File Upload</Card.Title>
                <Form className="form__el">
                    <Form.Label className="mt-4  col-lg-5" style={{ width: "100%" }}>
                        Attach Document
                        <Dropzone disabled={false} multiple={false} getData={getFileData} />
                    </Form.Label>
                </Form>
            </Card>
        </Row>
    );
};

export default SubmitClaim;

function InvoiceDetails({ existingData, sendData, id, add = () => {}, setTimeStamp = () => {} }) {
    return (
        <Form className="form__el">
            <Form.Label className="mt-4  col-md-3">
                Invoice No. <span className="mandate_field">*</span>
                <Form.Control
                    type="text"
                    onWheel={(e) => e.preventDefault()}
                    defaultValue=""
                    /*value={existingData.invoicenumber}*/ placeholder="Enter Invoice No."
                    onChange={(e) => sendData("invoicenumber", e.target.value, id)}
                />
            </Form.Label>{" "}
            <Form.Label className="mt-4  col-md-3">
                Invoice Date <span className="mandate_field">*</span>
                <Form.Control
                    type="date"
                    defaultValue=""
                    /*value={existingData.invoicedate}*/ placeholder="Enter Invoice Date."
                    onChange={(e) => {
                        sendData("invoicedate", `${formatDate__ddMMyyyy(e.target.value)} ${new Date().toTimeString().substring(0, 8)}`, id);
                    }}
                />
            </Form.Label>{" "}
            <Form.Label className="mt-4  col-md-3">
                Invoice Amount <span className="mandate_field">*</span>
                <Form.Control
                    type="number"
                    onWheel={(e) => e.preventDefault()}
                    defaultValue=""
                    /*value={existingData.invoiceamount}*/ placeholder="Enter Invoice "
                    onChange={(e) => sendData("invoiceamount", e.target.valueAsNumber, id)}
                />
            </Form.Label>{" "}
            <Button className="mt-5 next_btn" style={{ marginLeft: "unset" }} onClick={add}>
                Add
            </Button>
        </Form>
    );
}

function InvoiceFormSnippet({ invoice, handleFields: sendData = () => {}, id, remove = () => {}, displayData, existingData, stepData }) {
    const [userDetails] = useState(JSON.parse(sessionStorage.getItem("user")));
    const [expenseType, setExpenseType] = useState([]);
    const [expenseTypeValue, setExpenseTypeValue] = useState("");
    const controllerRef = useRef();
    const formRef = useRef();
    const [snippetdata, setsnippetData] = useState();

    const { invoicenumber } = stepData;

    const { invoices, setInvoice } = useSnippetStore((state) => state);

    useEffect(() => {
        const data = invoices
            .map((item) => item.items)
            .flat(1)
            .filter((item) => item?.id === id);
        if (data?.length > 0) {
            setsnippetData(data[0]);
        }
    }, [invoices]);

    const getDropDownValues = async () => {
        const { providerId, tokenID, userCode, providerTypeID } = userDetails;

        const payLoadExpenseType = {
            tokenID: tokenID,
            providerID: providerId,
            userID: userCode,
            providerType: providerTypeID,
        };

        const resultExpenseType = await getExpenseTypeDropDownvalues(payLoadExpenseType);
        setExpenseType(getResultFromData(resultExpenseType));
    };

    const debouncedSearch = useCallback(
        _.debounce((value, callBack) => {
            if (controllerRef.current) {
                controllerRef.current.abort();
            }
            controllerRef.current = new AbortController();
            try {
                if (value.value.length > 0) {
                    const { providerId, tokenID, userCode } = userDetails;
                    const payLoad = {
                        tokenID: tokenID,
                        userID: userCode,
                        providerID: providerId,
                        groupCode: value.groupCode,
                        searchValue: value.value,
                    };
                    axios({
                        url: `${decideENV() === "DEV" ? import.meta.env.VITE_BaseURL_DEV : import.meta.env.VITE_BaseURL_PROD}/fetchingItem`,
                        method: "post",
                        data: payLoad,
                        signal: controllerRef.current.signal,
                        headers: {
                            "eO2-Secret-Code": import.meta.env.VITE_EO2_SECRET_CODE,
                            "Content-Type": "multipart/form-data",
                        },
                    }).then((data) => {
                        if (data.data.result) {
                            callBack(
                                data.data.result.map((item) => ({
                                    value: item.itemCode,
                                    label: item.itemName,
                                }))
                            );
                        } else {
                            cogoToast.error(data.data.message);
                        }
                    });
                }
            } catch (err) {
                console.log(err);
            }
        }, 1000),
        []
        // eslint-disable-next-line react-hooks/exhaustive-deps
    );

    const handleChange = (value, field, id, invoice) => {
        setInvoice(
            "items",
            {
                invoicenumber: invoicenumber,
                field: "itemname",
                value: value.label,
                id: id,
            },
            id
        );
        setInvoice(
            "items",
            {
                invoicenumber: invoicenumber,
                field: "itemcode",
                value: value.value,
                id: id,
            },
            id
        );
    };

    const customStyles = {
        menuList: (provided, state) => ({
            ...provided,
            marginBottom: 10,
        }),
    };

    useEffect(() => {
        getDropDownValues();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            ref={formRef}
            className="form__el shadow p-3 mb-5 bg-white rounded"
            style={{ padding: "0 1rem !important", marginTop: "1rem" }}
            id={`form_${id}`}
        >
            <Form.Label className="mt-4  col-lg-2">
                Expense Type <span className="mandate_field">*</span>
                <Form.Select
                    variant="primary"
                    name="expensetype"
                    value={snippetdata?.itemgroupcode || ""}
                    id="expense__type"
                    title="Select Expense Type"
                    onChange={(key) => {
                        setExpenseTypeValue(key.target.value);
                        sendData("itemgroupcode", key.target.value, id, invoice);
                        setInvoice(
                            "items",
                            {
                                invoicenumber: invoicenumber,
                                field: "itemgroupcode",
                                value: key.target.value,
                                id: id,
                            },
                            id
                        );
                    }}
                >
                    <option disabled value="">
                        Select
                    </option>
                    {expenseType?.map((et, key) => (
                        <option key={`expense_type_option_${key}`} value={et.serviceType}>
                            {et.serviceTypeDesc}
                        </option>
                    ))}
                </Form.Select>
            </Form.Label>{" "}
            <Form.Label className="mt-4  col-lg-2">
                Expense Code <span className="mandate_field">*</span>
                <div className="mb-3">
                    <AsyncSelect
                        key={`__asyncSelect__InvoiceFormSnippet__${id}`}
                        cacheOptions
                        styles={customStyles}
                        noOptionsMessage={() => "No results to display"}
                        onChange={(e) => handleChange(e, "expensecode", id, invoice)}
                        loadOptions={(value, callback) => debouncedSearch({ value: value, groupCode: expenseTypeValue }, callback)}
                        placeholder="Search Expense Code"
                        components={{ MenuList: CustomVirtualList }}
                    />
                </div>
            </Form.Label>{" "}
            <Form.Label className="mt-4  col-lg-2">
                Total Quantity <span className="mandate_field">*</span>
                <Form.Control
                    type="number"
                    onWheel={(e) => e.preventDefault()}
                    name="totalquantity"
                    value={snippetdata?.quantity || ""}
                    placeholder="Enter Total Quantity"
                    onChange={(e) => {
                        sendData("quantity", e.target.valueAsNumber, id, invoice);
                        setInvoice(
                            "items",
                            {
                                invoicenumber: invoicenumber,
                                field: "quantity",
                                value: e.target.valueAsNumber,
                                id: id,
                            },
                            id
                        );
                    }}
                />
            </Form.Label>{" "}
            <Form.Label className="mt-4  col-lg-2">
                Total Amount <span className="mandate_field">*</span>
                <Form.Control
                    type="number"
                    onWheel={(e) => e.preventDefault()}
                    name="totalamount"
                    value={snippetdata?.amount || ""}
                    placeholder="Enter Total Amount "
                    onChange={(e) => {
                        sendData("amount", e.target.valueAsNumber, id, invoice);
                        setInvoice(
                            "items",
                            {
                                invoicenumber: invoicenumber,
                                field: "amount",
                                value: e.target.valueAsNumber,
                                id: id,
                            },
                            id
                        );
                    }}
                />
            </Form.Label>{" "}
            <Button className="remove__btn" onClick={() => remove(id, invoice)}>
                X
            </Button>
        </Form>
    );
}
