import React, { createContext, useState } from "react";

export const openContext = createContext({});
const MenuBarContext = ({ children }) => {
    const [isOpen, setIsOpen] = useState(true);

    const toggleMenuBar = () => {
        setIsOpen(!isOpen);
    };
    return <openContext.Provider value={{ isOpen, toggleMenuBar }}>{children}</openContext.Provider>;
};

export default MenuBarContext;
